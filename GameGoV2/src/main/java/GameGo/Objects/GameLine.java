package GameGo.Objects;

import java.text.ParseException;
import java.util.Scanner;

public class GameLine {
    @SuppressWarnings("resource")
	public Game add(String line) throws ParseException {
        Game tmp = new Game();
        Scanner lineScanner = new Scanner(line);
        lineScanner.useDelimiter(",");
        tmp.setName(lineScanner.next());
        tmp.setPrice(Double.parseDouble(lineScanner.next()));
        tmp.setQuantity(Integer.valueOf(lineScanner.next()));
        String rating = lineScanner.next();
        if (rating.toLowerCase().equals("e_ten")){
            tmp.setRating(ESRBRating.E_TEN);
        } else if (rating.toLowerCase().equals("e_child")){
            tmp.setRating(ESRBRating.E_CHILD);
        } else if (rating.toLowerCase().equals("teen")){
            tmp.setRating(ESRBRating.TEEN);
        } else if (rating.toLowerCase().equals("mature")){
            tmp.setRating(ESRBRating.MATURE);
        } else if (rating.toLowerCase().equals("adult")){
            tmp.setRating(ESRBRating.ADULT);
        } else {
            tmp.setRating(ESRBRating.EVERYONE);
        }
        tmp.setConsoleType(lineScanner.next());

        return tmp;
    }
}
