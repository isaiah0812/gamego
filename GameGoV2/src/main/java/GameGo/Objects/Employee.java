package GameGo.Objects;

import java.text.DecimalFormat;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.sun.istack.logging.Logger;

import GameGo.Exceptions.*;
import GameGo.Models.EmployeeTableModel;

public class Employee extends Person {
	private Logger logger = Logger.getLogger(Employee.class);
	private boolean annualPay;
	private Date clockInTime;
	private String employeeID;
	private double payRate;
	private Map<String, Double> punchCard;
	private double totalHours;
	private Position position;
	private String password;

	public Employee() {
		clockInTime = new Date();
		punchCard = new HashMap<String, Double>();
	}

	@SuppressWarnings("deprecation")
	public Employee(String i, String l, String f, String p, String d, String g, String e, String ph, String aSN,
			String aC, String aS, String aZ, String pa, String mon, String tues, String wed, String thurs, String fri,
			String sat, String sun) throws PositionException, GenderException, EmailException, StateException {
		this.setEmployeeID(i);
		this.setLastName(l);
		this.setFirstName(f);
		switch (p.toLowerCase()) {
		case "cashier":
			this.setPosition(Position.CASHIER);
			this.setAnnualPay(false);
			break;
		case "manager":
			this.setPosition(Position.MANAGER);
			this.setAnnualPay(true);
			break;
		case "stocker":
			this.setPosition(Position.STOCKER);
			this.setAnnualPay(false);
			break;
		case "technician":
			this.setPosition(Position.TECHNICIAN);
			this.setAnnualPay(false);
			break;
		default:
			throw new PositionException();
		}

		this.setDob(new Date(Date.parse(d)));
		switch (g.toLowerCase()) {
		case "male":
			this.setGender(Gender.MALE);
			break;
		case "female":
			this.setGender(Gender.FEMALE);
			break;
		case "not specified":
			this.setGender(Gender.NOT_SPECIFIED);
			break;
		default:
			throw new GenderException();
		}

		if (EmployeeTableModel.Email.validateEmail(e)) {
			this.setEmail(e);
		} else {
			throw new EmailException();
		}

		this.setPhoneNumber(ph);
		Address addy = new Address();
		addy.setStreetName(aSN);
		addy.setCity(aC);
		addy.setState(null);
		for (int a = 0; a < State.values().length; a++) {
			if (aS.contains(State.values()[a].toString())) {
				addy.setState(State.values()[a]);
				break;
			}
		}

		if (addy.getState() == null) {
			throw new StateException();
		}

		addy.setZip(aZ);
		this.setAddress(addy);
		this.setPayRate(Double.parseDouble(pa));
		Map<String, Double> pc = new HashMap<String, Double>();
		pc.put("Monday", Double.valueOf(mon));
		pc.put("Tuesday", Double.valueOf(tues));
		pc.put("Wednesday", Double.valueOf(wed));
		pc.put("Thursday", Double.valueOf(thurs));
		pc.put("Friday", Double.valueOf(fri));
		pc.put("Saturday", Double.valueOf(sat));
		pc.put("Sunday", Double.valueOf(sun));
		this.setPunchCard(pc);
	}

	public Employee(String first, String last, String id, String password) {
		super(first, last);
		employeeID = id;
		this.password = password;
		clockInTime = new Date();
		punchCard = new HashMap<String, Double>();
	}

	public boolean isAnnualPay() {
		return annualPay;
	}

	public void setAnnualPay(boolean annualPay) {
		this.annualPay = annualPay;
	}

	public Date getClockInTime() {
		return clockInTime;
	}

	public void setClockInTime(Date clockInTime) {
		this.clockInTime = clockInTime;
	}

	public String getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(String employeeID) {
		this.employeeID = employeeID;
	}

	public double getPayRate() {
		return payRate;
	}

	public void setPayRate(double payRate) {
		this.payRate = payRate;
	}

	public Map<String, Double> getPunchCard() {
		return punchCard;
	}

	public void setPunchCard(Map<String, Double> punchCard) {
		this.punchCard = punchCard;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public int getPassword() {
		return password.hashCode();
	}
	
	public String getPasswordString() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public void clockIn() {
		Instant i = Instant.now();
		clockInTime.setTime(i.toEpochMilli());
		logger.info(this.employeeID + " (" + this.getLastName() + ", " + this.getFirstName() + ") - Clock In: "
				+ clockInTime.getTime());
	}

	@SuppressWarnings("deprecation")
	@Override
	public void clockOut() {
		Instant i = Instant.now();
		Date d = new Date(i.toEpochMilli());
		Double entry = Double.valueOf((double) ((d.getTime() - clockInTime.getTime()) / 3600000.0));

		String day = null;
		switch (d.getDay()) {
		case 0:
			day = "Sunday";
			break;
		case 1:
			day = "Monday";
			break;
		case 2:
			day = "Tuesday";
			break;
		case 3:
			day = "Wednesday";
			break;
		case 4:
			day = "Thursday";
			break;
		case 5:
			day = "Friday";
			break;
		case 6:
			day = "Saturday";
			break;
		default:
			break;
		}

		logger.info(this.employeeID + " (" + this.getLastName() + ", " + this.getFirstName() + ") - Clock Out: " + day
				+ " " + new DecimalFormat(".##").format(entry.doubleValue()) + "hrs");
		punchCard.put(day, entry.doubleValue());
		totalHours += entry.doubleValue();
		logger.info(this.employeeID + " (" + this.getLastName() + ", " + this.getFirstName() + ") - Total Hours: " + this.getTotalHours());
	}

	public double getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(double totalHours) {
		this.totalHours = totalHours;
	}

	
}
