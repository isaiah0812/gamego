package GameGo.Objects;

public enum Position {
	CASHIER("Cashier"),
	MANAGER("Manager"),
	STOCKER("Stocker"),
	TECHNICIAN("Technician");
	
	@SuppressWarnings("unused")
	private String name;
	
	private Position(String name) {
		this.name = name;
	}
}
