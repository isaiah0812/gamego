//package GameGo.Objects;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Scanner;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import GameGo.Exceptions.EmailException;
//import GameGo.Exceptions.GenderException;
//import GameGo.Exceptions.PositionException;
//import GameGo.Exceptions.StateException;
//import GameGo.Models.ConcreteTableModel;
//
//public class EmployeeRecord extends ConcreteTableModel {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//
//	public EmployeeRecord(File dataFile) {
//		super(dataFile);
//		// GameGo.Objects.Address, City, GameGo.Objects.State, and Zip are combined in file as just GameGo.Objects.Address
//		String [] cn = {"ID", "Last Name", "First Name", "GameGo.Objects.Position", "DOB", "GameGo.Objects.Gender", "Email", "Phone Number", "GameGo.Objects.Address", "City", "GameGo.Objects.State", "Zip", "Pay Rate", "Hours", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
//		this.columnNames = cn;
//	}
//
//	@Override
//	public boolean isCellEditable(int row, int col) {
//		// TODO Auto-generated method stub
//		return true;
//	}
//
//	@Override
//	public void populateTable() {
//		Scanner in = null;
//		try {
//			in = new Scanner(dataFile);
//			if(in.hasNext()) {
//				in.nextLine();
//			}
//			
//			while(in.hasNext()) {
//				String [] info = in.nextLine().split(",");
//				
//				if(info.length == 20) {
//					this.data.add(info);
//				}
//				else {
//					System.err.println("Data missing");
//				}
//			}
//		} catch(IOException e) {
//			System.err.println(e);
//		} finally {
//			if(in != null) {
//				in.close();
//			}
//		}
//	}
//
//	@Override
//	public void setValueAt(Object value, int row, int col) {
//		data.get(row)[col] = value.toString();
//	}
//
//	@Override
//	public void saveData() {
//		// TODO Auto-generated method stub
//		
//	}
//	
//	@SuppressWarnings("deprecation")
//	public Employee objectVersion(String id) throws PositionException, GenderException, EmailException, StateException {
//		Employee emp = null;
//		for(int i = 0; i < data.size(); i++) {
//			if(data.get(i)[0].equals(id)) {
//				emp = new Employee();
//				emp.setEmployeeID(id);
//				emp.setLastName(data.get(i)[1]);
//				emp.setFirstName(data.get(i)[2]);
//				switch(data.get(i)[3].toLowerCase()) {
//					case "cashier": emp.setPosition(Position.CASHIER);
//									emp.setAnnualPay(false);
//									break;
//					case "manager": emp.setPosition(Position.MANAGER);
//									emp.setAnnualPay(true);
//									break;
//					case "stocker": emp.setPosition(Position.STOCKER);
//									emp.setAnnualPay(false);
//									break;
//					case "technician": emp.setPosition(Position.TECHNICIAN);
//									emp.setAnnualPay(false);
//									break;
//					default: throw new PositionException();
//				}
//				emp.setDob(new Date(Date.parse(data.get(i)[4])));
//				switch(data.get(i)[5].toLowerCase()) {
//					case "male": emp.setGender(Gender.MALE);
//								 break;
//					case "female": emp.setGender(Gender.FEMALE);
//								 break;
//					case "not specified": emp.setGender(Gender.NOT_SPECIFIED);
//								 break;
//					default: throw new GenderException();
//				}
//				if(Email.validateEmail(data.get(i)[6])) {
//					emp.setEmail(data.get(i)[6]);
//				}
//				else {
//					throw new EmailException();
//				}
//				emp.setPhoneNumber(data.get(i)[7]);
//				Address addy = new Address();
//				addy.setStreetName(data.get(i)[8]);
//				addy.setCity(data.get(i)[9]);
//				for(int a = 0; a < State.values().length; a++) {
//					if(data.get(i)[10].equals(State.values()[a].toString())) {
//						addy.setState(State.values()[a]);
//					}
//					else {
//						addy.setState(null);
//					}
//				}
//				if(addy.getState() == null) {
//					throw new StateException();
//				}
//				addy.setZip(data.get(i)[11]);
//				emp.setAddress(addy);
//				emp.setPayRate(Double.parseDouble(data.get(i)[12]));
//				Map<String, Double> pc = new HashMap<String, Double>();
//				pc.put("Monday", Double.valueOf(data.get(i)[13]));
//				pc.put("Tuesday", Double.valueOf(data.get(i)[14]));
//				pc.put("Wednesday", Double.valueOf(data.get(i)[15]));
//				pc.put("Thursday", Double.valueOf(data.get(i)[16]));
//				pc.put("Friday", Double.valueOf(data.get(i)[17]));
//				pc.put("Saturday", Double.valueOf(data.get(i)[18]));
//				pc.put("Sunday", Double.valueOf(data.get(i)[19]));
//				emp.setPunchCard(pc);
//				break;
//			}
//		}
//		
//		return emp;
//	}
//	
//	public static class Email {
//		
//		public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
//				"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Za-z]{2,6}$", Pattern.CASE_INSENSITIVE);
//		
//		public static boolean validateEmail(String url) {
//			Matcher m = VALID_EMAIL_ADDRESS_REGEX.matcher(url);
//			if(m.matches()) {
//				return true;
//			}
//			
//			return false;
//		}
//	}
//
//}
