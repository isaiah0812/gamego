package GameGo.Objects;

import java.awt.*;

@SuppressWarnings("serial")
public class SplashScreen extends Frame {

    public SplashScreen(){
        init();
    }

    private void init(){
        this.setLayout(new BorderLayout());
        this.setSize(new Dimension(750, 750));
        this.setLocationRelativeTo(null);

        @SuppressWarnings("unused")
		java.awt.SplashScreen splash = java.awt.SplashScreen.getSplashScreen();


        this.setVisible(true);
    }
}
