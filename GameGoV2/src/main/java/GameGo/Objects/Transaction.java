package GameGo.Objects;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Transaction {
	private Logger logger = Logger.getLogger(Transaction.class.getName());
	private boolean online;
	private String receiptNumber;
	private double taxTotal;
	private double total;
	private List<Product> cart;
	private double paid;
	private Date transactionDate;
	
	public Transaction() {
		cart = new ArrayList<Product>();
		taxTotal = 0;
		total = 0;
		online = false;
		paid = 0;
		receiptNumber = generateReceiptNumber();
		transactionDate = new Date();
	}
	public boolean isOnline() {
		return online;
	}
	public void setOnline(boolean online) {
		this.online = online;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public double getTaxTotal() {
		this.taxTotal = this.total*.0825;
		return Double.parseDouble(new DecimalFormat(".##").format(taxTotal));
	}
	public void setTaxTotal(double taxTotal) {
		this.taxTotal = taxTotal;
	}
	public double getTotal() {
		return Double.parseDouble(new DecimalFormat(".##").format(total));
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public List<Product> getCart() {
		return cart;
	}
	public void setCart(List<Product> cart) {
		this.cart = cart;
	}
	
	public void addProduct(Product product) {
		this.total += product.getPrice();
		this.cart.add(product);
	}
	
	public double getGrandTotal() {
		return Double.parseDouble(new DecimalFormat(".##").format(this.getTaxTotal() + this.getTotal()));
	}
	
	public double payment(double amt) {
		paid += this.getGrandTotal() - amt;
		return paid;
	}
	
	public double getPaid() {
		return Double.parseDouble(new DecimalFormat(".##").format(paid));
	}
	
	public void setPaid(double paid) {
		this.paid = paid;
	}
	
	public Date getTransactionDate() {
		return transactionDate;
	}
	
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	
	private String generateReceiptNumber() {
		String receipt = "";
		
		for(int i = 0; i < 10; i++) {
			int val = (int) ((Math.round(Math.random() * 100) % 10));
			receipt += String.valueOf(val);
		}
		
		return receipt;
	}
	
	public static boolean validateReceiptNumber(String receipt) {
		Pattern validPattern = Pattern.compile("[0-9]{10}");
		Matcher m = validPattern.matcher(receipt);
		
		return m.matches();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cart == null) ? 0 : cart.hashCode());
		result = prime * result + (online ? 1231 : 1237);
		long temp;
		temp = Double.doubleToLongBits(paid);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((receiptNumber == null) ? 0 : receiptNumber.hashCode());
		temp = Double.doubleToLongBits(taxTotal);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(total);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((transactionDate == null) ? 0 : transactionDate.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (cart == null) {
			if (other.cart != null)
				return false;
		} else if (!cart.equals(other.cart))
			return false;
		if (online != other.online)
			return false;
		if (Double.doubleToLongBits(paid) != Double.doubleToLongBits(other.paid))
			return false;
		if (receiptNumber == null) {
			if (other.receiptNumber != null)
				return false;
		} else if (!receiptNumber.equals(other.receiptNumber))
			return false;
		if (Double.doubleToLongBits(taxTotal) != Double.doubleToLongBits(other.taxTotal))
			return false;
		if (Double.doubleToLongBits(total) != Double.doubleToLongBits(other.total))
			return false;
		if (transactionDate == null) {
			if (other.transactionDate != null)
				return false;
		} else if (!transactionDate.equals(other.transactionDate))
			return false;
		return true;
	}
	
	@SuppressWarnings("deprecation")
	public String printReceipt() {
		String receipt = "";
		
		receipt += "Receipt Number: " + this.receiptNumber + "Date: " 
		+ this.transactionDate.toLocaleString() + "\nProducts:\n-------------------\n";
		for(Product p: this.cart) {
			receipt += p.toString() + "\n";
		}
		receipt += "-------------------\nSales Tax = " + this.getTaxTotal() 
				+ "\nSubtotal = " + this.getTotal() + "\nGrand Total = " 
				+ this.getGrandTotal() + "\nChange = " 
				+ (paid - this.getGrandTotal());
		logger.info(receipt);
		return receipt;
	}
}
