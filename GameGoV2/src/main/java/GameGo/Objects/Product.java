package GameGo.Objects;

public class Product {
	private String name;
	private Double price;
	private int quantity;
	private Double consumerRating;
	private String imageName;

	public Product(){

	}

	Product(String Name, Double Price, int Quantity, Double Rating, String imageFileName){
		name = Name;
		price = Price;
		quantity = Quantity;
		consumerRating = Rating;
		imageName = imageFileName;
	}
	
	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public int orderProduct() {
		/////////////////////Make it do something//////////////////////
		return quantity;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Double getConsumerRating() {
		return consumerRating;
	}
	public void setConsumerRating(Double consumerRating) {
		this.consumerRating = consumerRating;
	}
	
	@Override
	public String toString() {
		String value = "Name: " + getName();
		value = value + ", Price: " + getPrice().toString();
		value = value + ", Quantity: " + Integer.toString(getQuantity());
		value = value + ", Rating: " + getConsumerRating();
		return value;
	}
}
