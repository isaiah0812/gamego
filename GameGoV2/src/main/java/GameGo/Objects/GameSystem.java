package GameGo.Objects;

public class GameSystem extends Product {
	private String serialNumber;
	
	public GameSystem() {
		super();
	}

	GameSystem(String Name, Double Price, int Quantity, Double Rating, String imageFileName) {
		super(Name, Price, Quantity, Rating, imageFileName);
		// TODO Auto-generated constructor stub
		serialNumber = generateSerialNumber();
	}

	private String generateSerialNumber() {
		// format - 000-000-000-000
		// meaning - Type-Model-Quantity(Place in GameGo.Objects.Inventory Slots)-Size(HardDrive)
		return "000-000-000-000";
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
}
