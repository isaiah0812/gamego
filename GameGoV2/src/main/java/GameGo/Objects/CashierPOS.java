package GameGo.Objects;

import java.util.logging.Logger;

import GameGo.Exceptions.POSException;

public class CashierPOS extends POS {
	
	private boolean open;
	private Logger logger = Logger.getLogger(CashierPOS.class.getName());
	
	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}
	
	public Account getAccount() {
		return this.account;
		
	}

	@Override
	public void startTransaction() throws POSException {
		if (open) {
			do {
				currentTrans = new Transaction();
			} while (account.findTransaction(currentTrans.getReceiptNumber()));
		}
		else {
			throw new POSException();
		}
	}

	public double finishTransaction(double pay) {
		// Will always return 0
		logger.info("Customer paid $" + pay);
		return currentTrans.payment(pay);
	}
}