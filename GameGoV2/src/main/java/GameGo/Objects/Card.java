package GameGo.Objects;

public class Card {
	private String cardNumber;
	private String cvcCode;
	private boolean debitCard;
	private String expDate;
	private String pin;
	private String zipCode;

	public Card(){

	}

	public Card(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getCvcCode() {
		return cvcCode;
	}
	public void setCvcCode(String cvcCode) {
		this.cvcCode = cvcCode;
	}
	public boolean isDebitCard() {
		return debitCard;
	}
	public void setDebitCard(boolean debitCard) {
		this.debitCard = debitCard;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	
}
