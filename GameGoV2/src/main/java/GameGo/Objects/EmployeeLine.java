package GameGo.Objects;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.logging.Logger;

import GameGo.Exceptions.EmailException;

public class EmployeeLine {
	final private Logger logger = Logger.getLogger(EmployeeLine.class.getName());
	@SuppressWarnings("resource")
	public Employee add(String line) throws ParseException, EmailException {
		Employee tmp = new Employee();
		Scanner lineScanner = new Scanner(line);
		lineScanner.useDelimiter(",");
		String temp[] = line.split(",");

		if (temp != null) {
			tmp.setEmployeeID(temp[0]);
			tmp.setFirstName(temp[1]);
			tmp.setLastName(temp[2]);
			String emailString = temp[3];
			if (emailString.matches("^.+@\\w+.\\D{2,}$")) {
				try {
					tmp.setEmail(emailString);
				} catch (EmailException e) {
					// TODO Auto-generated catch block
					String error = "";
					for(int i = 0; i < e.getStackTrace().length; i++) {
						error += e.getStackTrace()[i] + "\n";
					}
					logger.warning(error);
				}
			} else {
				try {
					tmp.setEmail("[Invalid Email]");
				} catch (EmailException e) {
					// TODO Auto-generated catch block
					String error = "";
					for(int i = 0; i < e.getStackTrace().length; i++) {
						error += e.getStackTrace()[i] + "\n";
					}
					logger.warning(error);
				}
			}
			String genderString = temp[4];
			if (genderString.toLowerCase().equals("male")) {
				tmp.setGender(Gender.MALE);
			} else if (genderString.toLowerCase().equals("female")) {
				tmp.setGender(Gender.FEMALE);
			} else {
				tmp.setGender(Gender.NOT_SPECIFIED);
			}
			tmp.setAddress(new Address(temp[5]));
			tmp.setDob(new SimpleDateFormat("MM/dd/yy").parse(temp[6]));
			tmp.setPhoneNumber(temp[7]);
			tmp.setPayRate(Double.valueOf(temp[8]));
			tmp.setPassword(temp[9]);
			String positionString = temp[10];
			if (positionString.toLowerCase().equals("manager")) {
				tmp.setPosition(Position.MANAGER);
			} else if (positionString.toLowerCase().equals("stocker")) {
				tmp.setPosition(Position.STOCKER);
			} else if (positionString.toLowerCase().equals("cashier")) {
				tmp.setPosition(Position.CASHIER);
			} else if (positionString.toLowerCase().equals("technician")) {
				tmp.setPosition(Position.TECHNICIAN);
			}
			tmp.setTotalHours(Double.valueOf(temp[11]).doubleValue());
		}
		return tmp;
	}
}
