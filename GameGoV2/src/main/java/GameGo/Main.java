package GameGo;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            @SuppressWarnings("unused")
			MainWindow window = new MainWindow();
        });
    }
}
