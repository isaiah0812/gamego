package GameGo.Models;

import GameGo.Objects.Employee;
import GameGo.Objects.Person;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class PersonTableModel extends AbstractTableModel {
    private String[] columnNames = {"Name", "Email", "Phone Number", "GameGo.Objects.Address", "GameGo.Objects.Gender", "DOB"};
    private ArrayList<Person> data;

    public PersonTableModel(ArrayList<Person> data) {
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return (data.get(rowIndex).getFirstName() + " " + data.get(rowIndex).getLastName());
        } else if (columnIndex == 1) {
            return data.get(rowIndex).getEmail();
        } else if (columnIndex == 2) {
            return data.get(rowIndex).getPhoneNumber();
        } else if (columnIndex == 3) {
            return data.get(rowIndex).getAddress().toString();
        } else if (columnIndex == 4) {
            return data.get(rowIndex).getGender().toString();
        } else if (columnIndex == 5) {
            return data.get(rowIndex).getDob().toString();
        }
        else {
            return "Null";
        }
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Class getColumnClass(int column) {
//        switch (column) {
//            case 0:
//                return String.class;
//            case 1:
//                return String.class;
//            case 2:
//                return String.class;
//            case 3:
//                return String.class;
//            case 4:
//                return String.class;
//            case 5:
//                return String.class;
//            case 6:
//                return String.class;
//            default:
//                return Double.class;
//        }
        return String.class;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    public void addRowFromTable(Employee person) {
        if(person != null) {
        	data.add(person);
        } else {
        	throw new NullPointerException();
        }
        this.fireTableDataChanged();
    }

    public void deleteRow(int selectedRow) {
        data.remove(selectedRow);
        this.fireTableDataChanged();
    }
}
