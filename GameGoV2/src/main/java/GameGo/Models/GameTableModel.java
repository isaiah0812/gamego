package GameGo.Models;

import GameGo.Objects.ESRBRating;
import GameGo.Objects.Game;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class GameTableModel extends AbstractTableModel {
    private String[] columnNames = {"Name", "Price", "Quantity", "Rating", "Console", "Serial Number"};
    private ArrayList<Game> data;

    public GameTableModel(ArrayList<Game> data) {
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return data.get(rowIndex).getName();
        } else if (columnIndex == 1) {
            return data.get(rowIndex).getPrice();
        } else if (columnIndex == 2) {
            return data.get(rowIndex).getQuantity();
        } else if (columnIndex == 3) {
            return data.get(rowIndex).getRating();
        } else if (columnIndex == 4) {
            return data.get(rowIndex).getConsoleType();
        } else if (columnIndex == 5) {
            return data.get(rowIndex).getSerialNumber();
        } else {
            return "Null";
        }
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Class getColumnClass(int column) {
        switch (column) {
            case 0:
                return String.class;
            case 1:
                return Double.class;
            case 2:
                return String.class;
            case 3:
                return ESRBRating.class;
            case 4:
                return String.class;
            case 5:
                return String.class;
            default:
                return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    public void addRowFromTable(Game game) {
        if(game != null) {
        	data.add(game);
        } else {
        	throw new NullPointerException();
        }
        this.fireTableDataChanged();
    }

    public void deleteRow(int selectedRow) {
        data.remove(selectedRow);
        this.fireTableDataChanged();
    }
}
