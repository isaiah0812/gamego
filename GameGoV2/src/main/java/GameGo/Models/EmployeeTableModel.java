package GameGo.Models;

import GameGo.Objects.Account;
import GameGo.Objects.Employee;

import javax.swing.table.AbstractTableModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmployeeTableModel extends AbstractTableModel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String[] columnNames = {"Name", "Email", "Phone Number", "Address", "Gender", "ID", "DOB", "Pay Rate", "Total Hours"};
    private ArrayList<Employee> data;
    private Account account = Account.getInstance();
    final private Logger logger = Logger.getLogger(EmployeeTableModel.class.getName());

    public EmployeeTableModel(ArrayList<Employee> data) {
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return (data.get(rowIndex).getFirstName() + " " + data.get(rowIndex).getLastName());
        } else if (columnIndex == 1) {
            return data.get(rowIndex).getEmail();
        } else if (columnIndex == 2) {
            return data.get(rowIndex).getPhoneNumber();
        } else if (columnIndex == 3) {
            return data.get(rowIndex).getAddress();
        } else if (columnIndex == 4) {
            return data.get(rowIndex).getGender().toString();
        } else if (columnIndex == 5) {
            return data.get(rowIndex).getEmployeeID();
        } else if (columnIndex == 6) {
            SimpleDateFormat fDate = new SimpleDateFormat("MM/dd/yy");
            return fDate.format(data.get(rowIndex).getDob());
        } else if (columnIndex == 7) {
            return data.get(rowIndex).getPayRate();
        } else if (columnIndex == 8) {
        	return data.get(rowIndex).getTotalHours();
        }
        else {
            return "Null";
        }
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Class getColumnClass(int column) {
//        switch (column) {
//            case 0:
//                return String.class;
//            case 1:
//                return String.class;
//            case 2:
//                return String.class;
//            case 3:
//                return String.class;
//            case 4:
//                return String.class;
//            case 5:
//                return String.class;
//            default:
//                return String.class;
//        }
        return String.class;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    public void addRowFromTable(Employee person) {
        if(person != null) {
        	data.add(person);
        } else {
        	throw new NullPointerException();
        }
        this.fireTableDataChanged();
    }

    public void deleteRow(int selectedRow) {
        data.remove(selectedRow);
        this.fireTableDataChanged();
    }

    public static class Email {
		
		public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
				"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Za-z]{2,6}$", Pattern.CASE_INSENSITIVE);
		
		public static boolean validateEmail(String url) {
			Matcher m = VALID_EMAIL_ADDRESS_REGEX.matcher(url);
			if(m.matches()) {
				return true;
			}
			
			return false;
		}
	}
    
    public void payDay() {
    	PrintWriter writer = null;
    	try {
    		writer = new PrintWriter(new File(System.getProperty("user.dir") + "/src/main/resources/employee_data.csv"));
    		writer.println("employee_id,first_name,last_name,email,gender,address,dob,phone,payRate,password,position,total_hours");
			for (Employee e : data) {
				account.payEmployee(e);
				e.setTotalHours(0);
				String info = String.join(",", e.getEmployeeID(),
											   e.getFirstName(),
											   e.getLastName(),
											   e.getEmail(),
											   String.valueOf(e.getGender()),
											   e.getAddress().getStreetName(),
											   new SimpleDateFormat("MM/dd/yyyy").format(e.getDob()),
											   e.getPhoneNumber(),
											   String.valueOf(e.getPayRate()),
											   e.getPasswordString(),
											   String.valueOf(e.getPosition()),
											   String.valueOf(e.getTotalHours()));
				writer.println(info);
			}
		} catch(FileNotFoundException e) {
			String error = "";
			for(int i = 0; i < e.getStackTrace().length; i++) {
				error += e.getStackTrace()[i];
			}
			logger.warning(error);
		} finally {
			if(writer != null) {
				writer.close();
			}
		}
    	
    }
}
