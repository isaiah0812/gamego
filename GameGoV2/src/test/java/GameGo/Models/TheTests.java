package GameGo.Models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import GameGo.Exceptions.*;
import GameGo.Objects.*;

class TheTests {
	
	@SuppressWarnings("unused")
	private ConcreteTableModel tableModel = null;
	
	@SuppressWarnings("unused")
	@DisplayName("Customer Creation Gender Fail")
	@Test
	void customerCreationGenderFail() {
		boolean caught = false;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "king";
		String number = "123456789010";
		String expire = "1118";
		String cvc = "333";
		String debit = "yes";
		String pin = "1234";
		
		try {
			Customer customer = new Customer(email,first,last,date,phone,
					street,city,state,zip,gender,number,expire,cvc,debit,pin,zip);
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = true;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		assertTrue(caught);
	}
	
	@SuppressWarnings("unused")
	@DisplayName("Customer Creation Gender Passes")
	@Test
	void customerCreationGenderPasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String number = "123456789010";
		String expire = "1118";
		String cvc = "333";
		String debit = "yes";
		String pin = "1234";
		
		try {
			Customer customer = new Customer(email,first,last,date,phone,
					street,city,state,zip,gender,number,expire,cvc,debit,pin,zip);
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		assertTrue(caught);
	}
	
	@SuppressWarnings("unused")
	@DisplayName("Customer Creation State Fails")
	@Test
	void customerCreationStateFails() {
		boolean caught = false;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "OZ";
		String zip = "76706";
		String gender = "male";
		String number = "123456789010";
		String expire = "1118";
		String cvc = "333";
		String debit = "yes";
		String pin = "1234";
		
		try {
			Customer customer = new Customer(email,first,last,date,phone,
					street,city,state,zip,gender,number,expire,cvc,debit,pin,zip);
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = true;
		}
		
		assertTrue(caught);
	}
	
	@SuppressWarnings("unused")
	@DisplayName("Customer Creation State Passes")
	@Test
	void customerCreationStatePasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String number = "123456789010";
		String expire = "1118";
		String cvc = "333";
		String debit = "yes";
		String pin = "1234";
		
		try {
			Customer customer = new Customer(email,first,last,date,phone,
					street,city,state,zip,gender,number,expire,cvc,debit,pin,zip);
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		assertTrue(caught);
	}
	
	@SuppressWarnings("unused")
	@DisplayName("Employee Creation State Fails")
	@Test
	void employeeCreationStateFails() {
		boolean caught = false;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "OZ";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		try {
			Employee employee = new Employee(id,last,first,position,date,gender,email,phone,
					street,city,state,zip,pay,
					mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = true;
		}
		
		assertTrue(caught);
	}
	
	@SuppressWarnings("unused")
	@DisplayName("Employee Creation Gender Fails")
	@Test
	void employeeCreationGenderFails() {
		boolean caught = false;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "king";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		try {
			Employee employee = new Employee(id,last,first,position,date,gender,email,phone,
					street,city,state,zip,pay,
					mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = true;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		assertTrue(caught);
	}
	
	@SuppressWarnings("unused")
	@DisplayName("Employee Creation Position Fails")
	@Test
	void employeeCreationPositionFails() {
		boolean caught = false;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "royalty";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		try {
			Employee employee = new Employee(id,last,first,position,date,gender,email,phone,
					street,city,state,zip,pay,
					mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = true;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		assertTrue(caught);
	}
	
	@SuppressWarnings("unused")
	@DisplayName("Employee Creation Email Fails")
	@Test
	void employeeCreationEmailFails() {
		boolean caught = false;
		String email = "SchaefferDuncan.baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		try {
			Employee employee = new Employee(id,last,first,position,date,gender,email,phone,
					street,city,state,zip,pay,
					mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = true;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		assertTrue(caught);
	}
	
	@DisplayName("Employee Creation Passes")
	@Test
	void employeeCreationPasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		@SuppressWarnings("unused")
		Employee employee = null;
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		assertTrue(caught);
	}
	
	@DisplayName("Employee's Address Update Passes")
	@Test
	void employeeAddressUpdatePasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee = null;
		
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		street = "1102 Speight Avenue";
		city = "Waco";
		zip = "76706";
		state = "TX";
		Address addy = new Address(street,city,state,zip);
		employee.setAddress(addy);
		
		assertTrue(caught && employee.getAddress().equals(addy));
	}
	
	@DisplayName("Employee's Email Update Passes")
	@Test
	void employeeEmailUpdates() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee = null;
		
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		try {
			employee.setEmail("schaefferduncan@gmail.com");
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		assertTrue(caught && employee.getEmail().equals("schaefferduncan@gmail.com"));
	}
	
	@DisplayName("Employee's Email Update Fails")
	@Test
	void employeeEmailUpdateFails() {
		boolean caught = false;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee = null;
		
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		try {
			employee.setEmail("schaefferduncan.gmail.com");
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = true;
		}
		
		assertTrue(caught && employee.getEmail().equals("SchaefferDuncan@baylor.edu"));
	}
	
	@DisplayName("Employee's Punch Card Update Passes")
	@Test
	void employeePunchCardUpdatePasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee = null;
		
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		Map<String,Double> pc = employee.getPunchCard();
		mon = "2";
		tues = "4";
		wed = "6";
		thurs = "8";
		fri = "10";
		sat = "12";
		sun = "14";
		pc.put("Monday", Double.valueOf(mon));
		pc.put("Tuesday", Double.valueOf(tues));
		pc.put("Wednesday", Double.valueOf(wed));
		pc.put("Thursday", Double.valueOf(thurs));
		pc.put("Friday", Double.valueOf(fri));
		pc.put("Saturday", Double.valueOf(sat));
		pc.put("Sunday", Double.valueOf(sun));
		employee.setPunchCard(pc);
		
		assertTrue(caught && employee.getPunchCard().equals(pc));
	}
	
	@DisplayName("Employee's First Name Update Passes")
	@Test
	void employeeFirstNameUpdatePasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee = null;
		
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		employee.setFirstName("King");
		
		assertTrue(caught && employee.getFirstName().equals("King"));
	}
	
	@DisplayName("Employee's Last Name Update Passes")
	@Test
	void employeeLastNameUpdatePasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee = null;
		
		
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		employee.setLastName("Rex");
		
		assertTrue(caught && employee.getLastName().equals("Rex"));
	}
	
	@SuppressWarnings("deprecation")
	@DisplayName("Employee's DOB Update Passes")
	@Test
	void employeeDOBUpdatePasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee = null;
		
		
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		employee.setDob(new Date(Date.parse("11/21/1995")));
		assertTrue(caught && employee.getDob().toString().equals(new Date(Date.parse("11/21/1995")).toString()));
	}
	
	@SuppressWarnings("deprecation")
	@DisplayName("Employee's DOB Update Fails")
	@Test
	void employeeDOBUpdateFails() {
		boolean caught = false;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee = null;
		
		
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		try {
			employee.setDob(new Date(Date.parse("Nov/21/1997boi")));
		} catch (IllegalArgumentException e) {
			caught = true;
		}
		
		assertTrue(caught && employee.getDob().toString().equals(new Date(Date.parse("11/21/1997")).toString()));
	}
	
	@DisplayName("Employee's Gender Update Passes")
	@Test
	void employeeGenderUpdatePasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee = null;
		
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		employee.setGender(Gender.NOT_SPECIFIED);
		
		assertTrue(caught && employee.getGender().toString().equals(Gender.NOT_SPECIFIED.toString()));
	}
	
	@DisplayName("Employee's Gender Update Fails")
	@Test
	void employeeGenderUpdateFails() {
		boolean caught = false;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee = null;
		
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		try {
			employee.setGender(Gender.valueOf(""));
		} catch(IllegalArgumentException e) {
			caught = true;
		}
		
		assertTrue(caught && employee.getGender().toString().equals(Gender.MALE.toString()));
	}
	
	@DisplayName("Employee's Pay Rate Updates")
	@Test
	void employeePayRateUpdates() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee = null;
		
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		employee.setPayRate(77);
		
		assertTrue(caught && employee.getPayRate() == 77);
	}
	
	@DisplayName("Employee's Position Updates")
	@Test
	void employeePositionUpdates() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee = null;
		
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		employee.setPosition(Position.TECHNICIAN);
		
		assertTrue(caught && employee.getPosition().toString().equals(Position.TECHNICIAN.toString()));
	}
	
	@DisplayName("Employee's Phone# Updates")
	@Test
	void employeePhoneNumberUpdates() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee = null;
		
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		employee.setPhoneNumber("5555555555");
		
		assertTrue(caught && employee.getPhoneNumber().equals("5555555555"));
	}
	
	@DisplayName("Employee's ID# Updates")
	@Test
	void employeeIDUpdates() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee = null;
		
		try {
			employee = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		employee.setEmployeeID("333");
		
		assertTrue(caught && employee.getEmployeeID().equals("333"));
	}
	
	@DisplayName("Game Rating Updates")
	@Test
	void gameRatingUpdates() {
		Game game = new Game("Tom Clancy's Rainbow Six Siege", 59.99, 1000000, 9.9, ESRBRating.MATURE, "img/r6", "XBOXONE");
		game.setRating(ESRBRating.TEEN);
		
		assertTrue(ESRBRating.TEEN.toString().equals(game.getRating().toString()));
	}
	
	@DisplayName("Game Price Updates")
	@Test
	void gamePriceUpdates() {
		Game game = new Game("Tom Clancy's Rainbow Six Siege", 59.99, 1000000, 9.9, ESRBRating.MATURE, "img/r6", "XBOXONE");
		game.setPrice(29.99);
		
		assertTrue(game.getPrice() == 29.99);
	}
	
	@DisplayName("Game Image Updates")
	@Test
	void gameImageUpdates() {
		Game game = new Game("Tom Clancy's Rainbow Six Siege", 59.99, 1000000, 9.9, ESRBRating.MATURE, "img/r6", "XBOXONE");
		game.setImageName("img/r6siege");
		
		assertTrue(game.getImageName().equals("img/r6siege"));
	}
	
	@DisplayName("Game Console Updates")
	@Test
	void gameConsoleUpdates() {
		Game game = new Game("Tom Clancy's Rainbow Six Siege", 59.99, 1000000, 9.9, ESRBRating.MATURE, "img/r6", "XBOXONE");
		game.setConsoleType("PC");
		
		assertTrue(game.getConsoleType().equals("PC"));
	}
	
	@DisplayName("Game Name Updates")
	@Test
	void gameNameUpdates() {
		Game game = new Game("Tom Clancy's Rainbow Six Siege", 59.99, 1000000, 9.9, ESRBRating.MATURE, "img/r6", "XBOXONE");
		game.setName("Schaeffer Duncan's Rainbow Six Siege");
		
		assertTrue(game.getName().equals("Schaeffer Duncan's Rainbow Six Siege"));
	}
	
	@DisplayName("Game Customer Rating Updates")
	@Test
	void gameCustomerRatingUpdates() {
		Game game = new Game("Tom Clancy's Rainbow Six Siege", 59.99, 1000000, 9.9, ESRBRating.MATURE, "img/r6", "XBOXONE");
		game.setConsumerRating(10.0);
		
		assertTrue(game.getConsumerRating() == 10.0);
	}
	
	@DisplayName("Game Table Model Insertion Passes")
	@Test
	void gameTableModelInsertionPasses() {
		Game game1 = new Game("Tom Clancy's Rainbow Six Siege", 59.99, 1000000, 9.9, ESRBRating.MATURE, "img/r6", "XBOXONE");
		Game game2 = new Game("Grand Theft Auto 5", 49.99, 2000000, 9.5, ESRBRating.MATURE, "img/gta5", "PC");
		Game game3 = new Game("Red Dead Redemption 2", 59.99, 5000000, 10.0, ESRBRating.MATURE, "img/rdr2", "PS4");
		ArrayList<Game> games = new ArrayList<Game>();
		games.add(game1);
		games.add(game2);
		GameTableModel gtm = new GameTableModel(games);
		int initial = gtm.getRowCount();
		try {
			gtm.addRowFromTable(game3);
		} catch(NullPointerException e) {
			initial = -1;
		}
		int post = gtm.getRowCount();
		assertEquals((initial+1),post);
	}
	
	@DisplayName("Game Table Model Insertion Fails")
	@Test
	void gameTableModelInsertionFails() {
		boolean caught = false;
		Game game1 = new Game("Tom Clancy's Rainbow Six Siege", 59.99, 1000000, 9.9, ESRBRating.MATURE, "img/r6", "XBOXONE");
		Game game2 = new Game("Grand Theft Auto 5", 49.99, 2000000, 9.5, ESRBRating.MATURE, "img/gta5", "PC");
		Game game3 = null;
		ArrayList<Game> games = new ArrayList<Game>();
		games.add(game1);
		games.add(game2);
		GameTableModel gtm = new GameTableModel(games);
		int initial = gtm.getRowCount();
		try {
			gtm.addRowFromTable(game3);
		} catch(NullPointerException e) {
			caught = true;
		}
		int post = gtm.getRowCount();
		assertTrue(initial == post && caught);
	}
	
	@DisplayName("Game Table Model Deletion Passes")
	@Test
	void gameTableModelDeletionPasses() {
		Game game1 = new Game("Tom Clancy's Rainbow Six Siege", 59.99, 1000000, 9.9, ESRBRating.MATURE, "img/r6", "XBOXONE");
		Game game2 = new Game("Grand Theft Auto 5", 49.99, 2000000, 9.5, ESRBRating.MATURE, "img/gta5", "PC");
		Game game3 = new Game("Red Dead Redemption 2", 59.99, 5000000, 10.0, ESRBRating.MATURE, "img/rdr2", "PS4");
		ArrayList<Game> games = new ArrayList<Game>();
		games.add(game1);
		games.add(game2);
		GameTableModel gtm = new GameTableModel(games);
		int initial = gtm.getRowCount();
		games.add(game3);
		gtm.deleteRow(1);
		int post = gtm.getRowCount();
		assertEquals(initial,post);
	}
	
	@DisplayName("Game Table Model Deletion Fails")
	@Test
	void gameTableModelDeletionFails() {
		boolean caught = false;
		Game game1 = new Game("Tom Clancy's Rainbow Six Siege", 59.99, 1000000, 9.9, ESRBRating.MATURE, "img/r6", "XBOXONE");
		Game game2 = new Game("Grand Theft Auto 5", 49.99, 2000000, 9.5, ESRBRating.MATURE, "img/gta5", "PC");
		Game game3 = new Game("Red Dead Redemption 2", 59.99, 5000000, 10.0, ESRBRating.MATURE, "img/rdr2", "PS4");
		ArrayList<Game> games = new ArrayList<Game>();
		games.add(game1);
		games.add(game2);
		games.add(game3);
		GameTableModel gtm = new GameTableModel(games);
		int initial = gtm.getRowCount();
		try {
			gtm.deleteRow(6);
		} catch (IndexOutOfBoundsException e) {
			caught = true;
		}
		int post = gtm.getRowCount();
		assertTrue(initial == post && caught);
	}
	
	@DisplayName("Game Table Model Item Access Passes")
	@Test
	void gameTableModelItemAccessPasses() {
		Game game1 = new Game("Tom Clancy's Rainbow Six Siege", 59.99, 1000000, 9.9, ESRBRating.MATURE, "img/r6", "XBOXONE");
		Game game2 = new Game("Grand Theft Auto 5", 49.99, 2000000, 9.5, ESRBRating.MATURE, "img/gta5", "PC");
		Game game3 = new Game("Red Dead Redemption 2", 59.99, 5000000, 10.0, ESRBRating.MATURE, "img/rdr2", "PS4");
		ArrayList<Game> games = new ArrayList<Game>();
		games.add(game1);
		games.add(game2);
		games.add(game3);
		GameTableModel gtm = new GameTableModel(games);
		assertTrue(gtm.getValueAt(2,0).equals("Red Dead Redemption 2"));
	}
	
	@DisplayName("Game Table Model Item Access Fails")
	@Test
	void gameTableModelItemAccessFails() {
		boolean caught = false;
		Game game1 = new Game("Tom Clancy's Rainbow Six Siege", 59.99, 1000000, 9.9, ESRBRating.MATURE, "img/r6", "XBOXONE");
		Game game2 = new Game("Grand Theft Auto 5", 49.99, 2000000, 9.5, ESRBRating.MATURE, "img/gta5", "PC");
		Game game3 = new Game("Red Dead Redemption 2", 59.99, 5000000, 10.0, ESRBRating.MATURE, "img/rdr2", "PS4");
		ArrayList<Game> games = new ArrayList<Game>();
		games.add(game1);
		games.add(game2);
		games.add(game3);
		GameTableModel gtm = new GameTableModel(games);
		try {
			gtm.getValueAt(3,0);
		} catch (IndexOutOfBoundsException e) {
			caught = true;
		}
		assertTrue(caught);
	}
	
	@DisplayName("Employee Table Model Insertion Passes")
	@Test
	void employeeTableModelInsertionPasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee1 = null;
		Employee employee2 = null;
		Employee employee3 = null;
		
		try {
			employee1 = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
			employee2 = employee1;
			employee2.setFirstName("King");
			employee2.setEmail("iamtheking@gmail.com");
			employee3 = employee2;
			employee3.setFirstName("Rex-Imperator");
			employee3.setLastName("Caesar");
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		ArrayList<Employee> employees = new ArrayList<Employee>();
		EmployeeTableModel etm = new EmployeeTableModel(employees);
		etm.addRowFromTable(employee1);
		etm.addRowFromTable(employee2);
		int initial = etm.getRowCount();
		etm.addRowFromTable(employee3);
		int post = etm.getRowCount();
		
		assertTrue((initial+1) == post && caught);
	}
	
	@DisplayName("Employee Table Model Insertion Fails")
	@Test
	void employeeTableModelInsertionFails() {
		boolean caught = false;
		Employee employee1 = null;
		
		ArrayList<Employee> employees = new ArrayList<Employee>();
		EmployeeTableModel etm = new EmployeeTableModel(employees);
		int initial = etm.getRowCount();
		try {
			etm.addRowFromTable(employee1);
		} catch(NullPointerException e) {
			caught = true;
		}
		int post = etm.getRowCount();
		
		assertTrue(initial == post && caught);
	}
	
	@DisplayName("Employee Table Model Deletion Passes")
	@Test
	void employeeTableModelDeletionPasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee1 = null;
		Employee employee2 = null;
		Employee employee3 = null;
		
		try {
			employee1 = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
			employee2 = employee1;
			employee2.setFirstName("King");
			employee2.setEmail("iamtheking@gmail.com");
			employee3 = employee2;
			employee3.setFirstName("Rex-Imperator");
			employee3.setLastName("Caesar");
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		ArrayList<Employee> employees = new ArrayList<Employee>();
		EmployeeTableModel etm = new EmployeeTableModel(employees);
		etm.addRowFromTable(employee1);
		etm.addRowFromTable(employee2);
		int initial = etm.getRowCount();
		etm.addRowFromTable(employee3);
		try {
			etm.deleteRow(1);
		} catch(IndexOutOfBoundsException e) {
			caught = false;
		}
		int post = etm.getRowCount();
		
		assertTrue(initial == post && caught);
	}
	
	@DisplayName("Employee Table Model Deletion Fails")
	@Test
	void employeeTableModelDeletionFails() {
		boolean caught = false;
		
		ArrayList<Employee> employees = new ArrayList<Employee>();
		EmployeeTableModel etm = new EmployeeTableModel(employees);
		try {
			etm.deleteRow(1);
		} catch(IndexOutOfBoundsException e) {
			caught = true;
		}
		
		assertTrue(caught);
	}
	
	@DisplayName("Employee Table Model Item Access Passes")
	@Test
	void employeeTableModelItemAccessPasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee1 = null;
		Employee employee2 = null;
		Employee employee3 = null;
		
		try {
			employee1 = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
			employee2 = employee1;
			employee2.setFirstName("King");
			employee2.setEmail("iamtheking@gmail.com");
			employee3 = employee2;
			employee3.setFirstName("Rex-Imperator");
			employee3.setLastName("Caesar");
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		ArrayList<Employee> employees = new ArrayList<Employee>();
		EmployeeTableModel etm = new EmployeeTableModel(employees);
		etm.addRowFromTable(employee1);
		etm.addRowFromTable(employee2);
		etm.addRowFromTable(employee3);
		String compareMe = null;
		try {
			compareMe =(String)etm.getValueAt(2, 0);
		} catch(IndexOutOfBoundsException e) {
			caught = false;
		}
		assertTrue(compareMe.equals("Rex-Imperator Caesar") && caught);
	}
	
	@DisplayName("Employee Table Model Item Access Fails")
	@Test
	void employeeTableModelItemAccessFails() {
		boolean caught = false;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee1 = null;
		Employee employee2 = null;
		Employee employee3 = null;
		
		try {
			employee1 = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
			employee2 = employee1;
			employee2.setFirstName("King");
			employee2.setEmail("iamtheking@gmail.com");
			employee3 = employee2;
			employee3.setFirstName("Rex-Imperator");
			employee3.setLastName("Caesar");
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		ArrayList<Employee> employees = new ArrayList<Employee>();
		EmployeeTableModel etm = new EmployeeTableModel(employees);
		etm.addRowFromTable(employee1);
		etm.addRowFromTable(employee2);
		etm.addRowFromTable(employee3);
		try {
			etm.getValueAt(3,0);
		} catch (IndexOutOfBoundsException e) {
			caught = true;
		}
		assertTrue(caught);
	}
	
	@DisplayName("Customer Table Model Insertion Passes")
	@Test
	void customerTableModelInsertionPasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String number = "123456789010";
		String expire = "1118";
		String cvc = "333";
		String debit = "yes";
		String pin = "1234";
		
		Customer customer1 = null;
		Customer customer2 = null;
		Customer customer3 = null;
		
		try {
			customer1 = new Customer(email,first,last,date,phone, street,city,state,zip,gender,number,expire,cvc,debit,pin,zip);
			customer2 = customer1;
			customer2.setFirstName("King");
			customer2.setEmail("iamtheking@gmail.com");
			customer3 = customer2;
			customer3.setFirstName("Rex-Imperator");
			customer3.setLastName("Caesar");
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		ArrayList<Customer> customers = new ArrayList<Customer>();
		CustomerTableModel ctm = new CustomerTableModel(customers);
		ctm.addRowFromTable(customer1);
		ctm.addRowFromTable(customer2);
		int initial = ctm.getRowCount();
		ctm.addRowFromTable(customer3);
		int post = ctm.getRowCount();
		assertTrue((initial+1) == post && caught);
	}
	
	@DisplayName("Customer Table Model Insertion Fails")
	@Test
	void customerTableModelInsertionFails() {
		boolean caught = false;
		Customer customer1 = null;
		
		ArrayList<Customer> customers = new ArrayList<Customer>();
		CustomerTableModel ctm = new CustomerTableModel(customers);
		
		int initial = ctm.getRowCount();
		try {
			ctm.addRowFromTable(customer1);
		} catch (NullPointerException e) {
			caught = true;
		}
		int post = ctm.getRowCount();
		
		assertTrue(initial == post && caught);
	}
	
	@DisplayName("Customer Table Model Deletion Passes")
	@Test
	void customerTableModelDeletionPasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String number = "123456789010";
		String expire = "1118";
		String cvc = "333";
		String debit = "yes";
		String pin = "1234";
		
		Customer customer1 = null;
		Customer customer2 = null;
		Customer customer3 = null;
		
		try {
			customer1 = new Customer(email,first,last,date,phone, street,city,state,zip,gender,number,expire,cvc,debit,pin,zip);
			customer2 = customer1;
			customer2.setFirstName("King");
			customer2.setEmail("iamtheking@gmail.com");
			customer3 = customer2;
			customer3.setFirstName("Rex-Imperator");
			customer3.setLastName("Caesar");
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		ArrayList<Customer> customers = new ArrayList<Customer>();
		CustomerTableModel ctm = new CustomerTableModel(customers);
		ctm.addRowFromTable(customer1);
		ctm.addRowFromTable(customer2);
		int initial = ctm.getRowCount();
		ctm.addRowFromTable(customer3);
		try {
			ctm.deleteRow(1);
		} catch (IndexOutOfBoundsException e) {
			caught = false;
		}
		int post = ctm.getRowCount();
		
		assertTrue(initial == post && caught);
	}
	
	@DisplayName("Customer Table Model Deletion Fails")
	@Test
	void customerTableModelDeletionFails() {
		boolean caught = false;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String number = "123456789010";
		String expire = "1118";
		String cvc = "333";
		String debit = "yes";
		String pin = "1234";
		
		Customer customer1 = null;
		Customer customer2 = null;
		Customer customer3 = null;
		
		try {
			customer1 = new Customer(email,first,last,date,phone, street,city,state,zip,gender,number,expire,cvc,debit,pin,zip);
			customer2 = customer1;
			customer2.setFirstName("King");
			customer2.setEmail("iamtheking@gmail.com");
			customer3 = customer2;
			customer3.setFirstName("Rex-Imperator");
			customer3.setLastName("Caesar");
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		ArrayList<Customer> customers = new ArrayList<Customer>();
		CustomerTableModel ctm = new CustomerTableModel(customers);
		
		int initial = ctm.getRowCount();
		try {
			ctm.deleteRow(2);
		} catch (IndexOutOfBoundsException e) {
			caught = true;
		}
		int post = ctm.getRowCount();
		
		assertTrue(initial == post && caught);
	}
	
	@DisplayName("Customer Table Model Item Access Passes")
	@Test
	void customerTableModelItemAccessPasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String number = "123456789010";
		String expire = "1118";
		String cvc = "333";
		String debit = "yes";
		String pin = "1234";
		
		Customer customer1 = null;
		Customer customer2 = null;
		Customer customer3 = null;
		
		try {
			customer1 = new Customer(email,first,last,date,phone, street,city,state,zip,gender,number,expire,cvc,debit,pin,zip);
			customer2 = customer1;
			customer2.setFirstName("King");
			customer2.setEmail("iamtheking@gmail.com");
			customer3 = customer2;
			customer3.setFirstName("Rex-Imperator");
			customer3.setLastName("Caesar");
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		ArrayList<Customer> customers = new ArrayList<Customer>();
		CustomerTableModel ctm = new CustomerTableModel(customers);
		ctm.addRowFromTable(customer1);
		ctm.addRowFromTable(customer2);
		ctm.addRowFromTable(customer3);
		
		String compareMe = null;
		try {
			compareMe = (String) ctm.getValueAt(2, 0);
		} catch (IndexOutOfBoundsException e) {
			caught = false;
		}
		
		assertTrue(compareMe.equals("Rex-Imperator Caesar") && caught);
	}
	
	@DisplayName("Customer Table Model Item Access Fails")
	@Test
	void customerTableModelItemAccessFails() {
		boolean caught = false;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String number = "123456789010";
		String expire = "1118";
		String cvc = "333";
		String debit = "yes";
		String pin = "1234";
		
		Customer customer1 = null;
		Customer customer2 = null;
		Customer customer3 = null;
		
		try {
			customer1 = new Customer(email,first,last,date,phone, street,city,state,zip,gender,number,expire,cvc,debit,pin,zip);
			customer2 = customer1;
			customer2.setFirstName("King");
			customer2.setEmail("iamtheking@gmail.com");
			customer3 = customer2;
			customer3.setFirstName("Rex-Imperator");
			customer3.setLastName("Caesar");
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		ArrayList<Customer> customers = new ArrayList<Customer>();
		CustomerTableModel ctm = new CustomerTableModel(customers);
		
		try {
			@SuppressWarnings("unused")
			String compareMe = (String) ctm.getValueAt(3, 0);
		} catch (IndexOutOfBoundsException e) {
			caught = true;
		}
		
		assertTrue(caught);
	}
	
	@DisplayName("Person Table Model Insertion Passes")
	@Test
	void personTableModelInsertionPasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee1 = null;
		Employee employee2 = null;
		Employee employee3 = null;
		
		try {
			employee1 = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
			employee2 = employee1;
			employee2.setFirstName("King");
			employee2.setEmail("iamtheking@gmail.com");
			employee3 = employee2;
			employee3.setFirstName("Rex-Imperator");
			employee3.setLastName("Caesar");
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		ArrayList<Person> employees = new ArrayList<Person>();
		PersonTableModel ptm = new PersonTableModel(employees);
		ptm.addRowFromTable(employee1);
		ptm.addRowFromTable(employee2);
		int initial = ptm.getRowCount();
		ptm.addRowFromTable(employee3);
		int post = ptm.getRowCount();
		
		assertTrue((initial+1) == post && caught);
	}
	
	@DisplayName("Person Table Model Insertion Fails")
	@Test
	void personTableModelInsertionFails() {
		boolean caught = false;
		Employee employee1 = null;
		
		ArrayList<Person> employees = new ArrayList<Person>();
		PersonTableModel ptm = new PersonTableModel(employees);
		int initial = ptm.getRowCount();
		try {
			ptm.addRowFromTable(employee1);
		} catch(NullPointerException e) {
			caught = true;
		}
		int post = ptm.getRowCount();
		
		assertTrue(initial == post && caught);
	}
	
	@DisplayName("Person Table Model Deletion Passes")
	@Test
	void personTableModelDeletionPasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee1 = null;
		Employee employee2 = null;
		Employee employee3 = null;
		
		try {
			employee1 = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
			employee2 = employee1;
			employee2.setFirstName("King");
			employee2.setEmail("iamtheking@gmail.com");
			employee3 = employee2;
			employee3.setFirstName("Rex-Imperator");
			employee3.setLastName("Caesar");
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		ArrayList<Person> employees = new ArrayList<Person>();
		PersonTableModel ptm = new PersonTableModel(employees);
		ptm.addRowFromTable(employee1);
		ptm.addRowFromTable(employee2);
		int initial = ptm.getRowCount();
		ptm.addRowFromTable(employee3);
		try {
			ptm.deleteRow(1);
		} catch(IndexOutOfBoundsException e) {
			caught = false;
		}
		int post = ptm.getRowCount();
		
		assertTrue(initial == post && caught);
	}
	
	@DisplayName("Person Table Model Deletion Fails")
	@Test
	void personTableModelDeletionFails() {
		boolean caught = false;
		
		ArrayList<Person> employees = new ArrayList<Person>();
		PersonTableModel ptm = new PersonTableModel(employees);
		try {
			ptm.deleteRow(1);
		} catch(IndexOutOfBoundsException e) {
			caught = true;
		}
		
		assertTrue(caught);
	}
	
	@DisplayName("Person Table Model Item Access Passes")
	@Test
	void personTableModelItemAccessPasses() {
		boolean caught = true;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee1 = null;
		Employee employee2 = null;
		Employee employee3 = null;
		
		try {
			employee1 = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
			employee2 = employee1;
			employee2.setFirstName("King");
			employee2.setEmail("iamtheking@gmail.com");
			employee3 = employee2;
			employee3.setFirstName("Rex-Imperator");
			employee3.setLastName("Caesar");
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		ArrayList<Person> employees = new ArrayList<Person>();
		PersonTableModel ptm = new PersonTableModel(employees);
		ptm.addRowFromTable(employee1);
		ptm.addRowFromTable(employee2);
		ptm.addRowFromTable(employee3);
		String compareMe = null;
		try {
			compareMe =(String)ptm.getValueAt(2, 0);
		} catch(IndexOutOfBoundsException e) {
			caught = false;
		}
		assertTrue(compareMe.equals("Rex-Imperator Caesar") && caught);
	}
	
	@DisplayName("Person Table Model Item Access Fails")
	@Test
	void personTableModelItemAccessFails() {
		boolean caught = false;
		String email = "SchaefferDuncan@baylor.edu";
		String first = "Schaeffer";
		String last = "Duncan";
		String date = "11/21/1997";
		String phone = "1234567890";
		String street = "One Bear Place";
		String city = "Waco";
		String state = "TX";
		String zip = "76706";
		String gender = "male";
		String id = "3";
		String position = "manager";
		String pay = "33";
		String mon = "3";
		String tues = "6";
		String wed = "9";
		String thurs = "12";
		String fri = "15";
		String sat = "18";
		String sun = "21";
		
		Employee employee1 = null;
		Employee employee2 = null;
		Employee employee3 = null;
		
		try {
			employee1 = new Employee(id,last,first,position,date,gender,email,phone,
												street,city,state,zip,pay,
												mon,tues,wed,thurs,fri,sat,sun);
			employee2 = employee1;
			employee2.setFirstName("King");
			employee2.setEmail("iamtheking@gmail.com");
			employee3 = employee2;
			employee3.setFirstName("Rex-Imperator");
			employee3.setLastName("Caesar");
		} catch (PositionException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (GenderException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			caught = false;
		} catch (StateException e) {
			// TODO Auto-generated catch block
			caught = false;
		}
		
		ArrayList<Person> employees = new ArrayList<Person>();
		PersonTableModel ptm = new PersonTableModel(employees);
		ptm.addRowFromTable(employee1);
		ptm.addRowFromTable(employee2);
		ptm.addRowFromTable(employee3);
		try {
			ptm.getValueAt(3,0);
		} catch (IndexOutOfBoundsException e) {
			caught = true;
		}
		assertTrue(caught);
	}
	
	// Game Table Model is the inventory and is reading from game_data.csv
	// Employee Table Model is reading from employee_data.csv
	// Customer Table Model is reading from customer_data.csv
	// Account object is reading from account.csv and so the transaction table model is using the account (singleton)
}