The full zipped file is on the website at:
www.se1gamego.weebly.com

The zipped file contains:
- domain model <- DONE
- requirements/tracability matrix
- fully dressed use cases
- system sequence diagrams for use cases
- user interface wireframes <- DONE
- operations (function comments)
- teamwork plan 
- presentation materials
- suggested point redistribution
- timecards report