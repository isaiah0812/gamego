package GameGo;

import GameGo.Exceptions.EmailException;
import GameGo.Exceptions.POSException;
import GameGo.Models.*;
import GameGo.Objects.*;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.WildcardFileFilter;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("serial")
public class MainWindow extends JFrame implements ActionListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame loginWindow;
    private JFrame addEmployeeWindow;
    private JFrame fireEmployeeWindow;
    private JFrame editEmployeeWindow;
    private JFrame addCustomerWindow;
    private JFrame editCustomerWindow;
    private JFrame deleteCustomerWindow;
    private JFrame idWindow;
    private JFrame nameWindow;
    private Container contentPane;
    private JPanel gamePoster;

    @SuppressWarnings("unused")
	private TableRowSorter<GameTableModel> sorter;

    //POS
    private POS pos = null;

    //Frames
    private JFrame currentPopUp = null;

    //Data
    private ArrayList<Employee> employees;
    private ArrayList<Game> games;
    private ArrayList<Transaction> transactions;
    private ArrayList<Customer> customers;
    private String username, password;

    // Tables and Table GameGo.Models
    private GameTableModel gameModel;
    private EmployeeTableModel employeeModel;
    private TransactionTableModel transactionModel;
    private CustomerTableModel customerModel;
    private JTable gameTable;
    private JTable employeeTable;
    private JTable customerTable;
    Employee employee1 = null;
    Customer customer1 = null;
    JTextField emailField;

    // add Employee variables
    Employee user = null;
    String first = "";
	String last = "";
	String id = "";
	String pw = "";
	String posi = "";
	String email = "";
	String rate = "";
	String dob = "";
	String adr = "";
	String phone = "";
	String gen = "";
	String card = "";
	String customerName = "";
	int index;

    // Flags
    boolean isManager = false;
    
    // Extra global variables
    private JTextField pay;

    // Test Data
    Employee testEmployee1 = new Employee("Manager", "Chipmunk", "manager", "password");
    Employee testEmployee2 = new Employee("Cashier", "Chipmunk", "cashier", "password");
    Employee testEmployee3 = new Employee("Theodore", "Chipmunk", "theodore", "chipmunk");
    
    // Logger
    final private Logger logger = Logger.getLogger(MainWindow.class.getName());

    public MainWindow() {
        super("GameGo v3.4");
        testEmployee1.setPosition(Position.MANAGER);
        try {
			testEmployee1.setEmail("manager@gamego.com");
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			String error = "";
			for(int i = 0; i < e.getStackTrace().length; i++) {
				error += e.getStackTrace()[i] + "\n";
			}
			logger.warning(error);
		}
        testEmployee1.setDob(new Date(Date.parse("08/12/98")));
        testEmployee2.setPosition(Position.CASHIER);
        try {
			testEmployee2.setEmail("manager@gamego.com");
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			String error = "";
			for(int i = 0; i < e.getStackTrace().length; i++) {
				error += e.getStackTrace()[i] + "\n";
			}
			logger.warning(error);
		}
        testEmployee2.setDob(new Date(Date.parse("08/12/98")));
        loadData();
        displayLogin();
        this.setSize(700, 500);
        this.setLocationRelativeTo(null); // Puts window in middle of screen
        //this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void loadData(){
        employees = new ArrayList<>();
        transactions = new ArrayList<>();
        games = new ArrayList<>();
        customers = new ArrayList<>();

        employees.add(testEmployee1);
        employees.add(testEmployee2);

        EmployeeLine employeeAdder = new EmployeeLine();
        CustomerLine customerAdder = new CustomerLine();
        GameLine gameAdder = new GameLine();
        try {
            @SuppressWarnings("resource")
			Scanner employeeScanner = new Scanner(new File(System.getProperty("user.dir") + "/src/main/resources/employee_data.csv"));
            employeeScanner.nextLine();
            while(employeeScanner.hasNextLine()){
                try {
					employees.add(employeeAdder.add(employeeScanner.nextLine()));
				} catch (EmailException e) {
					// TODO Auto-generated catch block
					String error = "";
					for(int i = 0; i < e.getStackTrace().length; i++) {
						error += e.getStackTrace()[i] + "\n";
					}
					logger.warning(error);
				}
            }

            @SuppressWarnings("resource")
			Scanner customerScanner = new Scanner(new File(System.getProperty("user.dir") + "/src/main/resources/customer_data.csv"));
            customerScanner.nextLine();
            while(customerScanner.hasNextLine()){
                customers.add(customerAdder.add(customerScanner.nextLine()));
            }

            @SuppressWarnings("resource")
			Scanner gameScanner = new Scanner(new File(System.getProperty("user.dir") + "/src/main/resources/game_data.csv"));
            gameScanner.nextLine();
            while(gameScanner.hasNextLine()){
                games.add(gameAdder.add(gameScanner.nextLine()));
            }
        } catch (FileNotFoundException | ParseException e){
        	String error = "";
			for(int i = 0; i < e.getStackTrace().length; i++) {
				error += e.getStackTrace()[i] + "\n";
			}
			logger.warning(error);
        }
    }

    public void displayLogin(){
        loginWindow = new JFrame("Login");
        Container mainPane = loginWindow.getContentPane();

        mainPane.setLayout(new GridLayout(0, 1, 5, 5));
        JLabel loginText = new JLabel("Welcome to GameGo");
        loginText.setHorizontalAlignment(JLabel.CENTER);
        JLabel userText = new JLabel("User:");
        userText.setHorizontalAlignment(JLabel.CENTER);
        JTextField userField = new JTextField();
        userField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                username = userField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                username = userField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                username = userField.getText();
            }
        });
        userField.setColumns(20);
        JPanel userPanel = new JPanel();
        userPanel.add(userField);
        JLabel passwordText = new JLabel("Password:");
        passwordText.setHorizontalAlignment(JLabel.CENTER);
        JPasswordField passwordField = new JPasswordField();
        passwordField.getDocument().addDocumentListener(new DocumentListener() {
            @SuppressWarnings("deprecation")
			@Override
            public void insertUpdate(DocumentEvent e) {
                password = passwordField.getText();
            }

            @SuppressWarnings("deprecation")
			@Override
            public void removeUpdate(DocumentEvent e) {
                password = passwordField.getText();
            }

            @SuppressWarnings("deprecation")
			@Override
            public void changedUpdate(DocumentEvent e) {
                password = passwordField.getText();
            }
        });
        passwordField.setColumns(20);
        JPanel passwordPanel = new JPanel();
        passwordPanel.add(passwordField);
        JButton loginButton = new JButton("Login");
        loginButton.addActionListener(this);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(loginButton);

        mainPane.add(loginText);
        mainPane.add(userText);
        mainPane.add(userPanel);
        mainPane.add(passwordText);
        mainPane.add(passwordPanel);
        mainPane.add(buttonPanel);

        loginWindow.setSize(300, 300);
        //loginWindow.pack();
        loginWindow.setLocationRelativeTo(null); // Puts window in middle of screen
        loginWindow.setVisible(true);
        loginWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public void initUI(){
    	this.getContentPane().removeAll();
        contentPane = this.getContentPane();
        contentPane.setLayout(new BorderLayout());

        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenuItem logOut = new JMenuItem("logout");
        logOut.addActionListener(this);
        fileMenu.add(logOut);
        menuBar.add(fileMenu);
        JMenu helpMenu = new JMenu("Help");
        menuBar.add(helpMenu);

        this.setJMenuBar(menuBar);

        JPanel gameScreen = new JPanel();
        gameScreen.setLayout(new BorderLayout());
        gamePoster = new JPanel();
        gamePoster.setLayout(new GridLayout(1, 4, 10, 10));
        //loadEmployeeView();
        if (isManager){
            loadManagerView();
        } else {
            loadEmployeeView();
        }
        //gameDescription.setColumns(this.getWidth() - 10);
        gameScreen.add(gamePoster, BorderLayout.CENTER);
        contentPane.add(gameScreen, BorderLayout.CENTER);
    }

    private void loadEmployeeView() {
    	pos = new CashierPOS();
    	((CashierPOS)pos).setOpen(true);
        try {
            BufferedImage myPicture;
            JLabel picLabel;
            File imageLocation = new File(System.getProperty("user.dir") + "/src/main/resources/employee");
            FileFilter pngOnly = new WildcardFileFilter("*.png", IOCase.INSENSITIVE);
            File[] images = imageLocation.listFiles(pngOnly);

            String[] titles = {"Time Card", "Transactions", "Inventory", "Customers"};
            for (int i = 0; i < images.length; i++) {

                myPicture = ImageIO.read(images[i]);
                picLabel = new JLabel(new ImageIcon(myPicture.getScaledInstance(100, 100, Image.SCALE_FAST)));
                JPanel actionGroup = new JPanel();
                actionGroup.setLayout(new GridLayout(2, 1, 1, 1));
                JButton actionButton = new JButton(titles[i]);
                actionButton.addActionListener(this);
                actionGroup.add(picLabel);
                actionGroup.add(actionButton);
                this.gamePoster.add(actionGroup);
            }
        } catch (Exception e) {
        	String error = "";
			for(int i = 0; i < e.getStackTrace().length; i++) {
				error += e.getStackTrace()[i];
			}
			logger.warning(error);
        }
    }

    private void loadManagerView() {
    	pos = new CashierPOS();
    	((CashierPOS)pos).setOpen(true);
        try {
            BufferedImage myPicture;
            JLabel picLabel;
            File imageLocation = new File(System.getProperty("user.dir") + "/src/main/resources/manager");
            FileFilter pngOnly = new WildcardFileFilter("*.png", IOCase.INSENSITIVE);
            File[] images = imageLocation.listFiles(pngOnly);
            String[] titles = {"Time Card", "Transactions", "Inventory", "Employee List", "Customers"};
            for (int i = 0; i < images.length; i++) {

                myPicture = ImageIO.read(images[i]);
                picLabel = new JLabel(new ImageIcon(myPicture.getScaledInstance(100, 100, Image.SCALE_FAST)));
                JPanel actionGroup = new JPanel();
                actionGroup.setLayout(new GridLayout(2, 1, 1, 1));
                JButton actionButton = new JButton(titles[i]);
                actionButton.addActionListener(this);
                actionGroup.add(picLabel);
                actionGroup.add(actionButton);
                this.gamePoster.add(actionGroup);
            }
        } catch (Exception e) {
        	String error = "";
			for(int i = 0; i < e.getStackTrace().length; i++) {
				error += e.getStackTrace()[i];
			}
			logger.warning(error);
        }
    }

    private void viewInventory(){
        gameModel = new GameTableModel(games);
        gameTable = new JTable(gameModel);
        gameTable.setPreferredScrollableViewportSize(new Dimension(500, 200));
        gameTable.setFillsViewportHeight(true);
        gameTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        gameTable.setAutoCreateRowSorter(true);
        JFrame gameTableWindow = new JFrame("Product Inventory");
        JScrollPane scrollPane = new JScrollPane(gameTable);
        gameTableWindow.add(scrollPane);
        gameTableWindow.setLocationRelativeTo(this); // Puts window in middle of screen
        gameTableWindow.setSize(1000, 500);
        gameTableWindow.setVisible(true);
    }
    
    private void viewTimeCard(){
        JFrame timecardTableWindow = new JFrame("Time Card");
        JButton CheckIn = new JButton("Clock In");
        CheckIn.addActionListener(this);
        JButton CheckOut = new JButton("Clock Out");
        CheckOut.addActionListener(this);
        JPanel actionGroup = new JPanel();
        actionGroup.setLayout(new GridLayout(1, 2, 1, 1));
        actionGroup.add(CheckIn);
        actionGroup.add(CheckOut);
        timecardTableWindow.add(actionGroup);
        timecardTableWindow.setLocationRelativeTo(this); // Puts window in middle of screen
        timecardTableWindow.setSize(1000, 500);
        timecardTableWindow.setVisible(true);
    }

    private void viewPeople(){
        employeeModel = new EmployeeTableModel(employees);
        employeeTable = new JTable(employeeModel);
        employeeTable.setPreferredScrollableViewportSize(new Dimension(500, 200));
        employeeTable.setFillsViewportHeight(true);
        employeeTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        employeeTable.setAutoCreateRowSorter(true);
        JFrame personTableWindow = new JFrame("Employees");
        JScrollPane scrollPane = new JScrollPane(employeeTable);
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
        JMenuItem hireEmployee = new JMenuItem("New Employee...");
        hireEmployee.addActionListener(this);
        JMenuItem payEmployee = new JMenuItem("Pay Employees");
        payEmployee.addActionListener(this);
        fileMenu.add(hireEmployee);
        fileMenu.add(payEmployee);
        menuBar.add(fileMenu);

        JMenu editMenu = new JMenu("Edit");
        JMenuItem editEmployee = new JMenuItem("Edit Employee...");
        editEmployee.addActionListener(this);
        JMenuItem fireEmployee = new JMenuItem("Fire Employee");
        fireEmployee.addActionListener(this);
        editMenu.add(editEmployee);
        editMenu.add(fireEmployee);
        menuBar.add(editMenu);
        personTableWindow.setJMenuBar(menuBar);
        personTableWindow.setLocationRelativeTo(this); // Puts window in middle of screen
        personTableWindow.add(scrollPane);
        personTableWindow.setSize(1000, 500);
        personTableWindow.setVisible(true);
    }

    private JFrame viewTransactions(){
    	if(currentPopUp != null) {
    		currentPopUp.removeAll();
    		currentPopUp.setVisible(false);
    	}
        Transaction test1 = new Transaction();
        transactions.add(test1);
        JFrame transactionWindow = new JFrame();
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
        JMenuItem newSale = new JMenuItem("New Sale...");
        newSale.addActionListener(this);
        fileMenu.add(newSale);
        menuBar.add(fileMenu);

        JMenu editMenu = new JMenu("Actions");
        JMenuItem returnItem = new JMenuItem("Return Item...");
        returnItem.addActionListener(this);
        editMenu.add(returnItem);
        menuBar.add(editMenu);

        transactionWindow.setJMenuBar(menuBar);
        transactionWindow.setSize(700, 500);
        transactionWindow.setLocationRelativeTo(this); // Puts window in middle of screen
        transactionModel = new TransactionTableModel();
        customerTable = new JTable(transactionModel);
        customerTable.setPreferredScrollableViewportSize(new Dimension(500, 200));
        customerTable.setFillsViewportHeight(true);
        customerTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        customerTable.setAutoCreateRowSorter(true);
        JScrollPane scrollPane = new JScrollPane(customerTable);
        transactionWindow.add(scrollPane);
        transactionWindow.setVisible(true);
        
        return transactionWindow;
    }
    
    private JFrame newTransaction() {
    	if(currentPopUp != null) {
    		currentPopUp.removeAll();
    		currentPopUp.setVisible(false);
    	}
    	if(pos != null) {
        	try {
				pos.startTransaction();
				JFrame newTransWindow = new JFrame("New Transaction");
				newTransWindow.setLayout(new BorderLayout());
				newTransWindow.setSize(700, 500);
				newTransWindow.setLocationRelativeTo(null);
				gameModel = new GameTableModel(games);
				gameTable = new JTable(gameModel);
				gameTable.setPreferredScrollableViewportSize(new Dimension(500, 200));
				gameTable.setFillsViewportHeight(true);
				gameTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				gameTable.setAutoCreateRowSorter(true);
				JScrollPane scrollPane = new JScrollPane(gameTable);
				newTransWindow.add(scrollPane, BorderLayout.NORTH);
				
				JPanel buttonPanel = new JPanel();
				buttonPanel.setLayout(new BorderLayout());
				JButton addProduct = new JButton("Add Product to Cart");
				addProduct.setSize(new Dimension(100, 40));
				addProduct.addActionListener(this);
				buttonPanel.add(addProduct, BorderLayout.WEST);
				JButton finishTransaction = new JButton("Finish Transaction");
				finishTransaction.addActionListener(this);
				finishTransaction.setSize(new Dimension(100, 40));
				buttonPanel.add(finishTransaction, BorderLayout.EAST);
				
				newTransWindow.add(buttonPanel, BorderLayout.SOUTH);
				newTransWindow.setVisible(true);
				
				return newTransWindow;
			} catch (POSException e) {
				// TODO Auto-generated catch block
				String error = "";
				for(int i = 0; i < e.getStackTrace().length; i++) {
					error += e.getStackTrace()[i];
				}
				logger.warning(error);
			}
    	}
    	
    	return null;
    }
    
    private JFrame finishTransaction() {
    	if(currentPopUp != null) {
    		currentPopUp.removeAll();
    		currentPopUp.setVisible(false);
    	}
    	JTextArea receipt = new JTextArea();
    	JPanel scrollPane = new JPanel();
    	scrollPane.setSize(new Dimension(500, 400));
    	receipt.setText(pos.getCurrentTrans().printReceipt());
    	receipt.setEditable(false);
    	scrollPane.add(receipt);
    	JPanel paymentPanel = new JPanel();
    	JLabel paymentLabel = new JLabel("Payment");
    	paymentPanel.add(paymentLabel);
    	pay = new JTextField();
    	pay.setPreferredSize(new Dimension(120, 20));
    	paymentPanel.add(pay);
    	JButton paymentButton = new JButton("Pay");
    	paymentButton.addActionListener(this);
    	paymentPanel.add(paymentButton);
    	JFrame concludeWindow = new JFrame("Receipt");
    	concludeWindow.setLayout(new BorderLayout());
    	concludeWindow.setSize(700, 500);
    	concludeWindow.setLocationRelativeTo(null);
    	concludeWindow.add(scrollPane, BorderLayout.NORTH);
    	concludeWindow.add(paymentPanel, BorderLayout.SOUTH);
    	concludeWindow.setVisible(true);
    	
    	return concludeWindow;
    }

    private void viewCustomers(){
        Customer test1 = new Customer();
        customers.add(test1);
        JFrame customerWindow = new JFrame();
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
        JMenuItem newCustomer = new JMenuItem("New Customer...");
        newCustomer.addActionListener(this);
        fileMenu.add(newCustomer);
        menuBar.add(fileMenu);

        JMenu editMenu = new JMenu("Edit");
        JMenuItem editCustomer = new JMenuItem("Edit Selected Customer...");
        editCustomer.addActionListener(this);
        JMenuItem deleteCustomer = new JMenuItem("Delete Selected Customer...");
        deleteCustomer.addActionListener(this);
        editMenu.add(editCustomer);
        editMenu.add(deleteCustomer);
        menuBar.add(editMenu);

        customerWindow.setJMenuBar(menuBar);
        customerWindow.setSize(700, 500);
        customerWindow.setLocationRelativeTo(this); // Puts window in middle of screen
        customerModel = new CustomerTableModel(customers);
        customerTable = new JTable(customerModel);
        customerTable.setPreferredScrollableViewportSize(new Dimension(500, 200));
        customerTable.setFillsViewportHeight(true);
        customerTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        customerTable.setAutoCreateRowSorter(true);
        JScrollPane scrollPane = new JScrollPane(customerTable);
        customerWindow.add(scrollPane);
        customerWindow.setVisible(true);
    }

    // Utilities Functions
    private Employee verifyCredentials (String userID, String passwordHash){
        for (Employee e : employees){
            if (e.getClass() == Employee.class){
                if (e.getEmployeeID().equals(userID)){
                    if (e.getPassword() == passwordHash.hashCode()){
                        return e;
                    } else {
                        return null;
                    }
                }
            }
        }
        return null;
    }

    // Not Finished
    @SuppressWarnings("unused")
	private <T> void viewObjects(AbstractTableModel model, JTable table, ArrayList<T> data){
        model = new PersonTableModel(new ArrayList<>());
        employeeTable = new JTable(gameModel);
        employeeTable.setPreferredScrollableViewportSize(new Dimension(500, 200));
        employeeTable.setFillsViewportHeight(true);
        employeeTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        employeeTable.setAutoCreateRowSorter(true);
        JFrame gameTableWindow = new JFrame("Title Library");
        JScrollPane scrollPane = new JScrollPane(gameTable);
        gameTableWindow.add(scrollPane);
        gameTableWindow.setSize(1000, 500);
        gameTableWindow.setVisible(true);
    }

    public void addEmployeeWindow() {
    	addEmployeeWindow = new JFrame("Add Employee");
        Container mainPane = addEmployeeWindow.getContentPane();

        mainPane.setLayout(new GridLayout(0, 1, 5, 5));
        JLabel labelText = new JLabel("Label: ");
        labelText.setHorizontalAlignment(JLabel.CENTER);
        JLabel firstText = new JLabel("First:");
        firstText.setHorizontalAlignment(JLabel.CENTER);
        JTextField firstField = new JTextField();
        firstField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                first = firstField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	first = firstField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	first = firstField.getText();
            }
        });
        firstField.setColumns(20);
        JPanel firstPanel = new JPanel();
        firstPanel.add(firstField);
        
        JLabel lastText = new JLabel("Last:");
        lastText.setHorizontalAlignment(JLabel.CENTER);
        JTextField lastField = new JTextField();
        lastField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                last = lastField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	last = lastField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	last = lastField.getText();
            }
        });

        lastField.setColumns(20);
        JPanel lastPanel = new JPanel();
        lastPanel.add(lastField);
        
        JLabel idText = new JLabel("ID:");
        idText.setHorizontalAlignment(JLabel.CENTER);
        JTextField idField = new JTextField();
        idField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                id = idField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	id = idField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	id = idField.getText();
            }
        });

        idField.setColumns(20);
        JPanel idPanel = new JPanel();
        idPanel.add(idField);
        
        JLabel passText = new JLabel("Password: ");
        passText.setHorizontalAlignment(JLabel.CENTER);
        JTextField passField = new JTextField();
        passField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                pw = passField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	pw = passField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	pw = passField.getText();
            }
        });

        passField.setColumns(20);
        JPanel passPanel = new JPanel();
        passPanel.add(passField);
        
        JLabel posText = new JLabel("Position: ");
        posText.setHorizontalAlignment(JLabel.CENTER);
        JTextField posField = new JTextField();
        posField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                posi = posField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	posi = posField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	posi = posField.getText();
            }
        });

        posField.setColumns(20);
        JPanel posPanel = new JPanel();
        posPanel.add(posField);

        JLabel emailText = new JLabel("Email: ");
        emailText.setHorizontalAlignment(JLabel.CENTER);
        JTextField emailField = new JTextField();
        emailField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	email = emailField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	email = emailField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	email = emailField.getText();
            }
        });

        emailField.setColumns(20);
        JPanel emailPanel = new JPanel();
        emailPanel.add(emailField);
        
        JLabel rateText = new JLabel("Rate: ");
        rateText.setHorizontalAlignment(JLabel.CENTER);
        JTextField rateField = new JTextField();
        rateField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	rate = rateField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	rate = rateField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	rate = rateField.getText();
            }
        });

        rateField.setColumns(20);
        JPanel ratePanel = new JPanel();
        ratePanel.add(rateField);
        
        JLabel dobText = new JLabel("Date of Birth: ");
        dobText.setHorizontalAlignment(JLabel.CENTER);
        JTextField dobField = new JTextField();
        dobField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	dob = dobField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	dob = dobField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	dob = dobField.getText();
            }
        });

        dobField.setColumns(20);
        JPanel dobPanel = new JPanel();
        dobPanel.add(dobField);
        
        JLabel adrText = new JLabel("Address: ");
        adrText.setHorizontalAlignment(JLabel.CENTER);
        JTextField adrField = new JTextField();
        adrField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	adr = adrField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	adr = adrField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	adr = adrField.getText();
            }
        });

        adrField.setColumns(20);
        JPanel adrPanel = new JPanel();
        adrPanel.add(adrField);
        
        JLabel phoneText = new JLabel("Phone#: ");
        phoneText.setHorizontalAlignment(JLabel.CENTER);
        JTextField phoneField = new JTextField();
        phoneField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	phone = phoneField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	phone = phoneField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	phone = phoneField.getText();
            }
        });

        phoneField.setColumns(20);
        JPanel phonePanel = new JPanel();
        phonePanel.add(phoneField);

        JLabel genText = new JLabel("Gender: ");
        genText.setHorizontalAlignment(JLabel.CENTER);
        JTextField genField = new JTextField();
        genField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	gen = genField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	gen = genField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	gen = genField.getText();
            }
        });

        genField.setColumns(20);
        JPanel genPanel = new JPanel();
        genPanel.add(genField);

        JButton addButton = new JButton("Add Employee");
        addButton.addActionListener(this);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(addButton);
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);
        buttonPanel.add(cancelButton);

        //mainPane.add(loginText);
        mainPane.add(firstText);
        mainPane.add(firstPanel);
        mainPane.add(lastText);
        mainPane.add(lastPanel);
        mainPane.add(idText);
        mainPane.add(idPanel);
        mainPane.add(passText);
        mainPane.add(passPanel);
        mainPane.add(posText);
        mainPane.add(posPanel);
        mainPane.add(emailText);
        mainPane.add(emailPanel);
        mainPane.add(rateText);
        mainPane.add(ratePanel);
        mainPane.add(dobText);
        mainPane.add(dobPanel);
        mainPane.add(adrText);
        mainPane.add(adrPanel);
        mainPane.add(phoneText);
        mainPane.add(phonePanel);
        mainPane.add(genText);
        mainPane.add(genPanel);
        mainPane.add(buttonPanel);

        addEmployeeWindow.setSize(300, 750);
        //loginWindow.pack();
        addEmployeeWindow.setLocationRelativeTo(null); // Puts window in middle of screen
        addEmployeeWindow.setVisible(true);
        addEmployeeWindow.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    public void addCustomerWindow() {
    	addCustomerWindow = new JFrame("Add Customer");
        Container mainPane = addCustomerWindow.getContentPane();

        mainPane.setLayout(new GridLayout(0, 1, 5, 5));
        JLabel firstText = new JLabel("First:");
        firstText.setHorizontalAlignment(JLabel.CENTER);
        JTextField firstField = new JTextField();
        firstField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                first = firstField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	first = firstField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	first = firstField.getText();
            }
        });
        firstField.setColumns(20);
        JPanel firstPanel = new JPanel();
        firstPanel.add(firstField);

        JLabel lastText = new JLabel("Last:");
        lastText.setHorizontalAlignment(JLabel.CENTER);
        JTextField lastField = new JTextField();
        lastField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                last = lastField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	last = lastField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	last = lastField.getText();
            }
        });

        lastField.setColumns(20);
        JPanel lastPanel = new JPanel();
        lastPanel.add(lastField);

        JLabel emailText = new JLabel("Email: ");
        emailText.setHorizontalAlignment(JLabel.CENTER);
        JTextField emailField = new JTextField();
        emailField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	email = emailField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	email = emailField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	email = emailField.getText();
            }
        });

        emailField.setColumns(20);
        JPanel emailPanel = new JPanel();
        emailPanel.add(emailField);

        JLabel dobText = new JLabel("Date of Birth: ");
        dobText.setHorizontalAlignment(JLabel.CENTER);
        JTextField dobField = new JTextField();
        dobField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	dob = dobField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	dob = dobField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	dob = dobField.getText();
            }
        });

        dobField.setColumns(20);
        JPanel dobPanel = new JPanel();
        dobPanel.add(dobField);

        JLabel adrText = new JLabel("Address: ");
        adrText.setHorizontalAlignment(JLabel.CENTER);
        JTextField adrField = new JTextField();
        adrField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	adr = adrField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	adr = adrField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	adr = adrField.getText();
            }
        });

        adrField.setColumns(20);
        JPanel adrPanel = new JPanel();
        adrPanel.add(adrField);
        
        JLabel phoneText = new JLabel("Phone#: ");
        phoneText.setHorizontalAlignment(JLabel.CENTER);
        JTextField phoneField = new JTextField();
        phoneField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	phone = phoneField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	phone = phoneField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	phone = phoneField.getText();
            }
        });

        phoneField.setColumns(20);
        JPanel phonePanel = new JPanel();
        phonePanel.add(phoneField);

        JLabel genText = new JLabel("Gender: ");
        genText.setHorizontalAlignment(JLabel.CENTER);
        JTextField genField = new JTextField();
        genField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	gen = genField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	gen = genField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	gen = genField.getText();
            }
        });

        genField.setColumns(20);
        JPanel genPanel = new JPanel();
        genPanel.add(genField);
        
        JLabel cardText = new JLabel("Card Number: ");
        cardText.setHorizontalAlignment(JLabel.CENTER);
        JTextField cardField = new JTextField();
        cardField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	card = cardField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	card = cardField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	card = cardField.getText();
            }
        });
        cardField.setColumns(20);
        JPanel cardPanel = new JPanel();
        cardPanel.add(cardField);

        JButton addButton = new JButton("Add Customer");
        addButton.addActionListener(this);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(addButton);
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);
        buttonPanel.add(cancelButton);

        mainPane.add(firstText);
        mainPane.add(firstPanel);
        mainPane.add(lastText);
        mainPane.add(lastPanel);
        mainPane.add(emailText);
        mainPane.add(emailPanel);
        mainPane.add(dobText);
        mainPane.add(dobPanel);
        mainPane.add(adrText);
        mainPane.add(adrPanel);
        mainPane.add(phoneText);
        mainPane.add(phonePanel);
        mainPane.add(genText);
        mainPane.add(genPanel);
        mainPane.add(cardText);
        mainPane.add(cardPanel);
        mainPane.add(buttonPanel);

        addCustomerWindow.setSize(300, 600);
        //loginWindow.pack();
        addCustomerWindow.setLocationRelativeTo(null); // Puts window in middle of screen
        addCustomerWindow.setVisible(true);
        addCustomerWindow.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    public void editEmployeeWindow(Employee employee) {
    	editEmployeeWindow = new JFrame("Edit Employee");
        Container mainPane = editEmployeeWindow.getContentPane();

        mainPane.setLayout(new GridLayout(0, 1, 5, 5));
        JLabel labelText = new JLabel("Label: ");
        labelText.setHorizontalAlignment(JLabel.CENTER);
        JLabel firstText = new JLabel("First:");
        firstText.setHorizontalAlignment(JLabel.CENTER);
        JTextField firstField = new JTextField();
        firstField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                employee.setFirstName(firstField.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	employee.setFirstName(firstField.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	employee.setFirstName(firstField.getText());
            }
        });
        firstField.setColumns(20);
        firstField.setText(employee.getFirstName());
        JPanel firstPanel = new JPanel();
        firstPanel.add(firstField);

        JLabel lastText = new JLabel("Last:");
        lastText.setHorizontalAlignment(JLabel.CENTER);
        JTextField lastField = new JTextField();
        lastField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	employee.setLastName(lastField.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	employee.setLastName(lastField.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	employee.setLastName(lastField.getText());
            }
        });

        lastField.setColumns(20);
        lastField.setText(employee.getLastName());
        JPanel lastPanel = new JPanel();
        lastPanel.add(lastField);
        
        JLabel idText = new JLabel("ID:");
        idText.setHorizontalAlignment(JLabel.CENTER);
        JTextField idField = new JTextField();
        idField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	employee.setEmployeeID(idField.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	employee.setEmployeeID(idField.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	employee.setEmployeeID(idField.getText());
            }
        });
        idField.setText(employee.getEmployeeID());
        idField.setColumns(20);
        JPanel idPanel = new JPanel();
        idPanel.add(idField);

        JLabel passText = new JLabel("Password: ");
        passText.setHorizontalAlignment(JLabel.CENTER);
        JTextField passField = new JTextField();
        passField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                employee.setPassword(passField.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	employee.setPassword(passField.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	employee.setPassword(passField.getText());
            }
        });
        passField.setText(String.valueOf(employee.getPasswordString()));
        passField.setColumns(20);
        JPanel passPanel = new JPanel();
        passPanel.add(passField);
        
        JLabel posText = new JLabel("Position: ");
        posText.setHorizontalAlignment(JLabel.CENTER);
        JTextField posField = new JTextField();
        posField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                posi = posField.getText().toLowerCase().trim();
                switch(posi) {
                case "manager":
                	employee.setPosition(Position.MANAGER);
                	break;
                case "cashier":
                	employee.setPosition(Position.CASHIER);
                	break;
                case "technician":
                	employee.setPosition(Position.TECHNICIAN);
                	break;
                case "stocker":
                	employee.setPosition(Position.STOCKER);
                	break;
                default:
                		employee.setPosition(Position.CASHIER);
                		break;
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	posi = posField.getText().toLowerCase().trim();
                switch(posi) {
                case "manager":
                	employee.setPosition(Position.MANAGER);
                	break;
                case "cashier":
                	employee.setPosition(Position.CASHIER);
                	break;
                case "technician":
                	employee.setPosition(Position.TECHNICIAN);
                	break;
                case "stocker":
                	employee.setPosition(Position.STOCKER);
                	break;
                default:
                		employee.setPosition(Position.CASHIER);
                		break;
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	posi = posField.getText().toLowerCase().trim();
                switch(posi) {
                case "manager":
                	employee.setPosition(Position.MANAGER);
                	break;
                case "cashier":
                	employee.setPosition(Position.CASHIER);
                	break;
                case "technician":
                	employee.setPosition(Position.TECHNICIAN);
                	break;
                case "stocker":
                	employee.setPosition(Position.STOCKER);
                	break;
                default:
                		employee.setPosition(Position.CASHIER);
                		break;
                }
            }
        });
        posField.setText(employee.getPosition().name());
        posField.setColumns(20);
        JPanel posPanel = new JPanel();
        posPanel.add(posField);

        JLabel emailText = new JLabel("Email: ");
        emailText.setHorizontalAlignment(JLabel.CENTER);
        emailField = new JTextField();
        emailField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	email = emailField.getText();
            	
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	email = emailField.getText();
            	
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	email = emailField.getText();
            	
            }
        });
        emailField.setText(employee.getEmail());
        emailField.setColumns(20);
        JPanel emailPanel = new JPanel();
        emailPanel.add(emailField);
        
        JLabel rateText = new JLabel("Rate: ");
        rateText.setHorizontalAlignment(JLabel.CENTER);
        JTextField rateField = new JTextField();
        rateField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	rate = rateField.getText();
            	employee.setPayRate(Double.parseDouble(rate));
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	rate = rateField.getText();
            	employee.setPayRate(Double.parseDouble(rate));
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	rate = rateField.getText();
            	employee.setPayRate(Double.parseDouble(rate));
            }
        });
        rateField.setText(String.valueOf(employee.getPayRate()));
        rateField.setColumns(20);
        JPanel ratePanel = new JPanel();
        ratePanel.add(rateField);
        
        JLabel dobText = new JLabel("Date of Birth: ");
        dobText.setHorizontalAlignment(JLabel.CENTER);
        JTextField dobField = new JTextField();
        dobField.getDocument().addDocumentListener(new DocumentListener() {
            @SuppressWarnings("deprecation")
			@Override
            public void insertUpdate(DocumentEvent e) {
            	dob = dobField.getText();
            	employee.setDob(new Date(Date.parse(dob)));
            }

            @SuppressWarnings("deprecation")
			@Override
            public void removeUpdate(DocumentEvent e) {
            	dob = dobField.getText();
            	employee.setDob(new Date(Date.parse(dob)));
            }

            @SuppressWarnings("deprecation")
			@Override
            public void changedUpdate(DocumentEvent e) {
            	dob = dobField.getText();
            	employee.setDob(new Date(Date.parse(dob)));
            }
        });
        dobField.setText(employee.getDob().toString());
        dobField.setColumns(20);
        JPanel dobPanel = new JPanel();
        dobPanel.add(dobField);
        
        JLabel adrText = new JLabel("Address: ");
        adrText.setHorizontalAlignment(JLabel.CENTER);
        JTextField adrField = new JTextField();
        adrField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	adr = adrField.getText();
            	String[] temp = adr.split(",");
            	Address address = new Address();
            	address.setStreetName(temp[0]);
            	address.setCity(temp[1]);
            	address.setState(State.TX);
            	address.setZip(temp[3]);
            	employee.setAddress(address);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	adr = adrField.getText();
            	String[] temp = adr.split(",");
            	Address address = new Address();
            	address.setStreetName(temp[0]);
            	address.setCity(temp[1]);
            	address.setState(State.TX);
            	address.setZip(temp[3]);
            	employee.setAddress(address);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	adr = adrField.getText();
            	String[] temp = adr.split(",");
            	Address address = new Address();
            	address.setStreetName(temp[0]);
            	address.setCity(temp[1]);
            	address.setState(State.TX);
            	address.setZip(temp[3]);
            	employee.setAddress(address);
            }
        });
        adrField.setText(employee.getAddress().toString());
        adrField.setColumns(20);
        JPanel adrPanel = new JPanel();
        adrPanel.add(adrField);
        
        JLabel phoneText = new JLabel("Phone#: ");
        phoneText.setHorizontalAlignment(JLabel.CENTER);
        JTextField phoneField = new JTextField();
        phoneField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	phone = phoneField.getText();
            	employee.setPhoneNumber(phone);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	phone = phoneField.getText();
            	employee.setPhoneNumber(phone);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	phone = phoneField.getText();
            	employee.setPhoneNumber(phone);
            }
        });
        phoneField.setText(employee.getPhoneNumber());
        phoneField.setColumns(20);
        JPanel phonePanel = new JPanel();
        phonePanel.add(phoneField);

        JLabel genText = new JLabel("Gender: ");
        genText.setHorizontalAlignment(JLabel.CENTER);
        JTextField genField = new JTextField();
        genField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	gen = genField.getText().toLowerCase();
            	Gender gender = null;
            	switch(gen) {
            	case "male":
            		gender = Gender.MALE;
            		break;
            	case "female":
            		gender = Gender.FEMALE;
            		break;
            	default:
            		gender = Gender.NOT_SPECIFIED;
            		break;
            	}
            	employee.setGender(gender);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	gen = genField.getText();
            	Gender gender = null;
            	switch(gen) {
            	case "male":
            		gender = Gender.MALE;
            		break;
            	case "female":
            		gender = Gender.FEMALE;
            		break;
            	default:
            		gender = Gender.NOT_SPECIFIED;
            		break;
            	}
            	employee.setGender(gender);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	gen = genField.getText();
            	Gender gender = null;
            	switch(gen) {
            	case "male":
            		gender = Gender.MALE;
            		break;
            	case "female":
            		gender = Gender.FEMALE;
            		break;
            	default:
            		gender = Gender.NOT_SPECIFIED;
            		break;
            	}
            	employee.setGender(gender);
            }
        });
        genField.setText(employee.getGender().name());
        genField.setColumns(20);
        JPanel genPanel = new JPanel();
        genPanel.add(genField);

        JButton addButton = new JButton("Edit Employee");
        addButton.addActionListener(this);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(addButton);
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);
        buttonPanel.add(cancelButton);

        //mainPane.add(loginText);
        mainPane.add(firstText);
        mainPane.add(firstPanel);
        mainPane.add(lastText);
        mainPane.add(lastPanel);
        mainPane.add(idText);
        mainPane.add(idPanel);
        mainPane.add(passText);
        mainPane.add(passPanel);
        mainPane.add(posText);
        mainPane.add(posPanel);
        mainPane.add(emailText);
        mainPane.add(emailPanel);
        mainPane.add(rateText);
        mainPane.add(ratePanel);
        mainPane.add(dobText);
        mainPane.add(dobPanel);
        mainPane.add(adrText);
        mainPane.add(adrPanel);
        mainPane.add(phoneText);
        mainPane.add(phonePanel);
        mainPane.add(genText);
        mainPane.add(genPanel);
        mainPane.add(buttonPanel);
        employee1 = employee;

        editEmployeeWindow.setSize(300, 750);
        //loginWindow.pack();
        editEmployeeWindow.setLocationRelativeTo(null); // Puts window in middle of screen
        editEmployeeWindow.setVisible(true);
        editEmployeeWindow.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    public void getIDWindow() {
    	idWindow = new JFrame("Enter ID");
    	Container mainPane = new Container();

    	mainPane.setLayout(new GridLayout(0,1,5,5));
    	JLabel idText = new JLabel("Enter Employee ID: ");
    	idText.setHorizontalAlignment(JLabel.CENTER);
    	JTextField idField = new JTextField();
    	idField.getDocument().addDocumentListener(new DocumentListener() {
    		 @Override
             public void insertUpdate(DocumentEvent e) {
                 id = idField.getText();
             }

             @Override
             public void removeUpdate(DocumentEvent e) {
             	id = idField.getText();
             }

             @Override
             public void changedUpdate(DocumentEvent e) {
             	id = idField.getText();
             }
    	});
    	idField.setColumns(20);
    	JPanel idPanel = new JPanel();
    	idPanel.add(idField);

    	JButton acceptButton = new JButton("Accept");
    	acceptButton.addActionListener(this);
    	JPanel buttonPanel = new JPanel();
        buttonPanel.add(acceptButton);

    	mainPane.add(idText);
    	mainPane.add(idPanel);
    	mainPane.add(buttonPanel);
    	idWindow.add(mainPane);

    	idWindow.setSize(300, 200);
    	idWindow.setLocationRelativeTo(this); // Puts window in middle of screen
    	idWindow.setVisible(true);
    	idWindow.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    public void editCustomerWindow(Customer customer) {
    	editCustomerWindow = new JFrame("Edit Customer");
        Container mainPane = editCustomerWindow.getContentPane();

        mainPane.setLayout(new GridLayout(0, 1, 5, 5));
        JLabel labelText = new JLabel("Label: ");
        labelText.setHorizontalAlignment(JLabel.CENTER);
        JLabel firstText = new JLabel("First:");
        firstText.setHorizontalAlignment(JLabel.CENTER);
        JTextField firstField = new JTextField();
        firstField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                customer.setFirstName(firstField.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	customer.setFirstName(firstField.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	customer.setFirstName(firstField.getText());
            }
        });
        firstField.setColumns(20);
        firstField.setText(customer.getFirstName());
        JPanel firstPanel = new JPanel();
        firstPanel.add(firstField);

        JLabel lastText = new JLabel("Last:");
        lastText.setHorizontalAlignment(JLabel.CENTER);
        JTextField lastField = new JTextField();
        lastField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	customer.setLastName(lastField.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	customer.setLastName(lastField.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	customer.setLastName(lastField.getText());
            }
        });

        lastField.setColumns(20);
        lastField.setText(customer.getLastName());
        JPanel lastPanel = new JPanel();
        lastPanel.add(lastField);
        
        JLabel emailText = new JLabel("Email: ");
        emailText.setHorizontalAlignment(JLabel.CENTER);
        emailField = new JTextField();
        emailField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	email = emailField.getText();
            	
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	email = emailField.getText();
            	
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	email = emailField.getText();
            	
            }
        });
        emailField.setText(customer.getEmail());
        emailField.setColumns(20);
        JPanel emailPanel = new JPanel();
        emailPanel.add(emailField);
        
        JLabel dobText = new JLabel("Date of Birth: ");
        dobText.setHorizontalAlignment(JLabel.CENTER);
        JTextField dobField = new JTextField();
        dobField.getDocument().addDocumentListener(new DocumentListener() {
            @SuppressWarnings("deprecation")
			@Override
            public void insertUpdate(DocumentEvent e) {
            	dob = dobField.getText();
            }

            @SuppressWarnings("deprecation")
			@Override
            public void removeUpdate(DocumentEvent e) {
            	dob = dobField.getText();
            }

            @SuppressWarnings("deprecation")
			@Override
            public void changedUpdate(DocumentEvent e) {
            	dob = dobField.getText();
            }
        });
        dobField.setText(customer.getDob().toString());
        dobField.setColumns(20);
        JPanel dobPanel = new JPanel();
        dobPanel.add(dobField);

        JLabel adrText = new JLabel("Address: ");
        adrText.setHorizontalAlignment(JLabel.CENTER);
        JTextField adrField = new JTextField();
        adrField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	adr = adrField.getText();
            	String[] temp = adr.split(",");
            	Address address = new Address();
            	address.setStreetName(temp[0]);
            	address.setCity(temp[1]);
            	address.setState(State.TX);
            	address.setZip(temp[3]);
            	customer.setAddress(address);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	adr = adrField.getText();
            	String[] temp = adr.split(",");
            	Address address = new Address();
            	address.setStreetName(temp[0]);
            	address.setCity(temp[1]);
            	address.setState(State.TX);
            	address.setZip(temp[3]);
            	customer.setAddress(address);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	adr = adrField.getText();
            	String[] temp = adr.split(",");
            	Address address = new Address();
            	address.setStreetName(temp[0]);
            	address.setCity(temp[1]);
            	address.setState(State.TX);
            	address.setZip(temp[3]);
            	customer.setAddress(address);
            }
        });
        adrField.setText(customer.getAddress().toString());
        adrField.setColumns(20);
        JPanel adrPanel = new JPanel();
        adrPanel.add(adrField);
        
        JLabel phoneText = new JLabel("Phone#: ");
        phoneText.setHorizontalAlignment(JLabel.CENTER);
        JTextField phoneField = new JTextField();
        phoneField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	phone = phoneField.getText();
            	customer.setPhoneNumber(phone);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	phone = phoneField.getText();
            	customer.setPhoneNumber(phone);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	phone = phoneField.getText();
            	customer.setPhoneNumber(phone);
            }
        });
        phoneField.setText(customer.getPhoneNumber());
        phoneField.setColumns(20);
        JPanel phonePanel = new JPanel();
        phonePanel.add(phoneField);

        JLabel genText = new JLabel("Gender: ");
        genText.setHorizontalAlignment(JLabel.CENTER);
        JTextField genField = new JTextField();
        genField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	gen = genField.getText().toLowerCase().trim();
            	Gender gender = null;
            	switch(gen) {
            	case "male":
            		gender = Gender.MALE;
            		break;
            	case "female":
            		gender = Gender.FEMALE;
            		break;
            	default:
            		gender = Gender.NOT_SPECIFIED;
            		break;
            	}
            	customer.setGender(gender);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	gen = genField.getText();
            	Gender gender = null;
            	switch(gen) {
            	case "male":
            		gender = Gender.MALE;
            		break;
            	case "female":
            		gender = Gender.FEMALE;
            		break;
            	default:
            		gender = Gender.NOT_SPECIFIED;
            		break;
            	}
            	customer.setGender(gender);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	gen = genField.getText();
            	Gender gender = null;
            	switch(gen) {
            	case "male":
            		gender = Gender.MALE;
            		break;
            	case "female":
            		gender = Gender.FEMALE;
            		break;
            	default:
            		gender = Gender.NOT_SPECIFIED;
            		break;
            	}
            	customer.setGender(gender);
            }
        });
        genField.setText(customer.getGender().name());
        genField.setColumns(20);
        JPanel genPanel = new JPanel();
        genPanel.add(genField);

        JLabel cardText = new JLabel("Card#: ");
        cardText.setHorizontalAlignment(JLabel.CENTER);
        JTextField cardField = new JTextField();
        cardField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
            	card = cardField.getText();
            	customer.setCard(new Card(card));
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	card = cardField.getText();
            	customer.setCard(new Card(card));
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	card = cardField.getText();
            	customer.setCard(new Card(card));
            }
        });
        cardField.setText(customer.getCard().getCardNumber());
        cardField.setColumns(20);
        JPanel cardPanel = new JPanel();
        cardPanel.add(cardField);

        JButton addButton = new JButton("Edit Customer");
        addButton.addActionListener(this);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(addButton);
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);
        buttonPanel.add(cancelButton);

        //mainPane.add(loginText);
        mainPane.add(firstText);
        mainPane.add(firstPanel);
        mainPane.add(lastText);
        mainPane.add(lastPanel);
        mainPane.add(emailText);
        mainPane.add(emailPanel);
        mainPane.add(dobText);
        mainPane.add(dobPanel);
        mainPane.add(adrText);
        mainPane.add(adrPanel);
        mainPane.add(phoneText);
        mainPane.add(phonePanel);
        mainPane.add(genText);
        mainPane.add(genPanel);
        mainPane.add(cardText);
        mainPane.add(cardPanel);
        mainPane.add(buttonPanel);
        customer1 = customer;

        editCustomerWindow.setSize(300, 800);
        //loginWindow.pack();
        editCustomerWindow.setLocationRelativeTo(null); // Puts window in middle of screen
        editCustomerWindow.setVisible(true);
        editCustomerWindow.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    public void nameAuthenticWindow(){
    	nameWindow = new JFrame("Enter First and Last Name");
    	Container mainPane = new Container();

    	mainPane.setLayout(new GridLayout(0,1,5,5));
    	JLabel customerNameText = new JLabel("Enter First and Last Name: ");
    	customerNameText.setHorizontalAlignment(JLabel.CENTER);
    	JTextField customerNameField = new JTextField();
    	customerNameField.getDocument().addDocumentListener(new DocumentListener() {
    		 @Override
             public void insertUpdate(DocumentEvent e) {
                 customerName = customerNameField.getText();
             }

             @Override
             public void removeUpdate(DocumentEvent e) {
            	 customerName = customerNameField.getText();
             }

             @Override
             public void changedUpdate(DocumentEvent e) {
            	 customerName = customerNameField.getText();
             }
    	});
    	customerNameField.setColumns(20);
    	JPanel customerNamePanel = new JPanel();
    	customerNamePanel.add(customerNameField);

    	JButton acceptButton = new JButton("Accept");
    	acceptButton.addActionListener(this);
    	JPanel buttonPanel = new JPanel();
        buttonPanel.add(acceptButton);

    	mainPane.add(customerNameText);
    	mainPane.add(customerNamePanel);
    	mainPane.add(buttonPanel);
    	nameWindow.add(mainPane);

    	nameWindow.setSize(300, 200);
    	nameWindow.setLocationRelativeTo(this); // Puts window in middle of screen
    	nameWindow.setVisible(true);
    	nameWindow.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }
    
    public void fireEmployeeWindow() {
    	fireEmployeeWindow = new JFrame("Fire Employee");
        Container mainPane = fireEmployeeWindow.getContentPane();

        mainPane.setLayout(new GridLayout(0, 1, 5, 5));
        JLabel idText = new JLabel("Employee ID: ");
        idText.setHorizontalAlignment(JLabel.CENTER);

        JTextField idField = new JTextField();
        idField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                id = idField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            	id = idField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            	id = idField.getText();
            }
        });
        idField.setColumns(20);
        JPanel idPanel = new JPanel();
        idPanel.add(idField);
        
        JButton fireButton = new JButton("Fire Employee");
        fireButton.addActionListener(this);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(fireButton);
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);
        buttonPanel.add(cancelButton);

        mainPane.add(idText);
        mainPane.add(idPanel);
        mainPane.add(buttonPanel);

        fireEmployeeWindow.setSize(300, 300);
        //loginWindow.pack();
        fireEmployeeWindow.setLocationRelativeTo(this); // Puts window in middle of screen
        fireEmployeeWindow.setVisible(true);
        fireEmployeeWindow.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    public void deleteCustomerWindow() {
    	deleteCustomerWindow = new JFrame("Delete Customer");
        Container mainPane = deleteCustomerWindow.getContentPane();

        mainPane.setLayout(new GridLayout(0, 1, 5, 5));

        JButton deleteButton = new JButton("Delete");
        deleteButton.addActionListener(this);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(deleteButton);
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);
        buttonPanel.add(cancelButton);

        mainPane.add(buttonPanel);

        deleteCustomerWindow.setSize(100, 150);
        //loginWindow.pack();
        deleteCustomerWindow.setLocationRelativeTo(this); // Puts window in middle of screen
        deleteCustomerWindow.setVisible(true);
        deleteCustomerWindow.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        deleteCustomerWindow.setAlwaysOnTop(true);
    }

    @SuppressWarnings("deprecation")
	@Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Login")){
            user = verifyCredentials(username, password);
            if (user != null){
                if (user.getPosition() == Position.MANAGER){
                    isManager = true;
                } else {
                	isManager = false;
                }
                initUI();
                loginWindow.dispose();
                this.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(new JFrame(), "Incorrect Username/Password");
            }
        }

        if (e.getActionCommand().equals("Inventory")){
            viewInventory();
        }

        if (e.getActionCommand().equals("Employee List")){
            viewPeople();
        }

        if (e.getActionCommand().equals("Transactions")){
            currentPopUp = viewTransactions();
        }

        if(e.getActionCommand().equals("Customers")){
            viewCustomers();
        }
        
        if(e.getActionCommand().equals("Time Card")){
            viewTimeCard();
        }

        if(e.getActionCommand().equals("Clock In")){
            user.clockIn();
        }

        if(e.getActionCommand().equals("Clock Out")){
            user.clockOut();
        }

        if(e.getActionCommand().equals("New Employee...")){
        	addEmployeeWindow();
        }
        
        if(e.getActionCommand().equals("logout")){
        	this.dispose();
        	displayLogin();
        }

        if(e.getActionCommand().equals("Add Employee")){
        	Employee temp = new Employee(first, last, id, pw);
        	String addr[] = adr.split(",");
        	temp.setAddress(new Address(addr[0], addr[1], "TX", addr[3]));
        	temp.setDob(new Date(Date.parse(dob)));
        	temp.setPhoneNumber(phone);
        	Position position = Position.MANAGER;
        	switch(posi.toLowerCase().trim()) {
        	case "manager":
        		position = Position.MANAGER;
        		break;
        	case "cashier":
        		position = Position.CASHIER;
        		break;
        	case "stocker":
        		position = Position.STOCKER;
        		break;
        	case "technician":
        		position = Position.TECHNICIAN;
        		break;
        		default:
        			break;
        	}
        	temp.setPosition(position);
        	temp.setPayRate(Double.parseDouble(rate));
        	try {
				temp.setEmail(email);
			} catch (EmailException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        	switch(gen) {
			case "male":
				temp.setGender(Gender.MALE);
				break;
			case "female":
				temp.setGender(Gender.FEMALE);
				break;
			default:
				temp.setGender(Gender.NOT_SPECIFIED);
				break;
			}
        	employees.add(temp);
        	addEmployeeWindow.dispose();
        }

        if(e.getActionCommand().equals("Edit Employee...")) {
        	getIDWindow();
        }

        if(e.getActionCommand().equals("Edit Employee")) {
        	try {
				employee1.setEmail(email);
				editEmployeeWindow.dispose();
				employeeModel.fireTableDataChanged();
			} catch (EmailException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				emailField.setText("");
			}
        }

        if(e.getActionCommand().equals("Accept")) {
        	if(idWindow != null) {
	        	for (Employee emp : employees){
	                if (emp.getClass() == Employee.class){
	                    if (emp.getEmployeeID().equals(id)){
	                    	editEmployeeWindow(emp);
	                    	idWindow.dispose();
	                        break;
	                    }
	                }
	            }
        	}
        }

        if(e.getActionCommand().equals("Fire Employee")) {
        	if(fireEmployeeWindow != null) {
        		if(fireEmployeeWindow.isActive()) {
	        		for(int i = 0; i < employees.size(); i++) {
	        			if(employees.get(i).getEmployeeID().equalsIgnoreCase(id)) {
	        				employees.remove(i);
	        				employeeModel.fireTableDataChanged();
	        				fireEmployeeWindow.dispose();
	        				break;
	        			}
	        		}

	        		// id the current user id is the one being deleted
	        		// then display the log in page to sign in again
	        		if(user.getEmployeeID().equalsIgnoreCase(id)) {
	        			displayLogin();
	        		}
        		} else {
        			fireEmployeeWindow();
        		}
        	} else {
        		fireEmployeeWindow();
        	}
        }

        if(e.getActionCommand().equals("New Customer...")) {
        	addCustomerWindow();
        }

        if(e.getActionCommand().equals("Add Customer")) {
        	Customer customer = new Customer();
        	Card c = new Card(card);
        	customer.setCard(c);
        	try {
				customer.setEmail(email);
			} catch (EmailException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        	customer.setFirstName(first);
        	customer.setLastName(last);
        	customer.setPhoneNumber(phone);
        	switch(gen) {
			case "male":
				customer.setGender(Gender.MALE);
				break;
			case "female":
				customer.setGender(Gender.FEMALE);
				break;
			default:
				customer.setGender(Gender.NOT_SPECIFIED);
				break;
			}
        	customer.setDob(new Date(Date.parse(dob)));
        	Address addr = new Address();
        	String[] temp = adr.split(",");
        	if(temp.length > 2) {
        	addr.setStreetName(temp[0]);
        	addr.setCity(temp[1]);
        	addr.setState(State.TX);
        	addr.setZip(temp[3]);
        	}
        	customer.setAddress(addr);

        	customers.add(customer);
        	addCustomerWindow.dispose();
        }

        if(e.getActionCommand().equals("Edit Selected Customer...")) {
        	index = customerTable.getSelectedRow();
        	if(customerTable.isRowSelected(index)) {
        		editCustomerWindow(customers.get(index));
        	}
        }
        
        if(e.getActionCommand().equals("Edit Customer")) {
        	customer1.setDob(new Date(Date.parse(dob)));
        	try {
				customer1.setEmail(email);
			} catch (EmailException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        	customerModel.fireTableDataChanged();
        	editCustomerWindow.dispose();
        }

        if(e.getActionCommand().equals("Delete Selected Customer...")) {
        	index = customerTable.getSelectedRow();
        	if(customerTable.isRowSelected(index)) {
	        	deleteCustomerWindow();
        	}
        }

        if(e.getActionCommand().equals("Delete")) {
        	customers.remove(index);
        	customerModel.fireTableDataChanged();
        	deleteCustomerWindow.dispose();
        }

        if(e.getActionCommand().equals("Cancel")) {
        	if(addEmployeeWindow != null && addEmployeeWindow.isActive()) {
        		addEmployeeWindow.dispose();
        	} else if(fireEmployeeWindow != null && fireEmployeeWindow.isActive()) {
        		fireEmployeeWindow.dispose();
        	} else if(deleteCustomerWindow != null && deleteCustomerWindow.isActive()) {
        		deleteCustomerWindow.dispose();
        	} else if(editEmployeeWindow != null && editEmployeeWindow.isActive()) {
        		editEmployeeWindow.dispose();
        	} else if(addCustomerWindow != null && addCustomerWindow.isActive()) {
        		addCustomerWindow.dispose();
        	} else if(editCustomerWindow != null && editCustomerWindow.isActive()) {
        		editCustomerWindow.dispose();
        	}
        }

        if(e.getActionCommand().equals("New Sale...")) {
        	currentPopUp = newTransaction();
        }

        if(e.getActionCommand().equals("Add Product to Cart")) {
        	Product p = new Game();
        	for(int i = 0; i < 6; i++) {
        		Object val = gameModel.getValueAt(gameTable.getSelectedRow(), i);
        		switch(i){
        			case 0: p.setName((String)val);
        					break;
        			case 1: p.setPrice((Double)val);
        					break;
        			case 2: p.setQuantity((int)val);
        					break;
        			case 3: ((Game)p).setRating((ESRBRating)val);
        					break;
        			case 4: ((Game)p).setConsoleType((String)val);
        					break;
        			default:
        		}
        	}

        	pos.getCurrentTrans().addProduct(p);
        }
        
        if(e.getActionCommand().equals("Finish Transaction")){
        	currentPopUp = finishTransaction();
        }
        
        if(e.getActionCommand().equals("Pay")) {
        	if(pay.getText() != null) {
            	Pattern digitPattern = Pattern.compile("[0-9]*.[0-9]{2}");
            	Matcher m = digitPattern.matcher(pay.getText());
            	
            	if(m.matches()) {
            		if(pos.finishTransaction(Double.parseDouble(pay.getText())) > 0) {
            			currentPopUp = finishTransaction();
            		}
            		else {
            			currentPopUp.removeAll();
            			currentPopUp.setVisible(false);
            			currentPopUp = null;
            			JOptionPane.showMessageDialog(new JFrame(), "Transaction Complete!\nThank you for shopping at GameGo!!!");
            			((CashierPOS)pos).getAccount().addTransaction(((CashierPOS)pos).getCurrentTrans());
            		}
            	}
        	}
        }
        
        if(e.getActionCommand().equals("Pay Employees")) {
        	logger.info("You have elected to pay the employees");
        	employeeModel.payDay();
        }
        
        if(e.getActionCommand().equals("Return Item...")) {
        	logger.info("You have elected to return a transaction");
        }
    }
}