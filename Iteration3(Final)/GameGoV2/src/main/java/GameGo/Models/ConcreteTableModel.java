package GameGo.Models;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public abstract class ConcreteTableModel extends AbstractTableModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected File dataFile;
	protected String [] columnNames;
	
	@Override
	public abstract int getColumnCount();

	@Override
	public abstract int getRowCount();

	@Override
	public abstract Object getValueAt(int row, int col);

	final public File getDataFile() {
		return dataFile;
	}

	final public void setDataFile(File dataFile) {
		this.dataFile = dataFile;
	}
	
	@Override
	public abstract boolean isCellEditable(int row, int col);
	
	public abstract void populateTable();
	
	public abstract void setValueAt(Object value, int row, int col);
	
	public abstract void saveData();
	
	public String[] getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
