package GameGo.Models;

import GameGo.Objects.Account;
import GameGo.Objects.Employee;
import GameGo.Objects.Transaction;

import javax.swing.table.AbstractTableModel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TransactionTableModel extends ConcreteTableModel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String[] columnNames = {"Transaction Date", "Receipt #", "Online", "Tax", "Subtotal", "Grand Total", "Total Payment"};
    private List<Transaction> data;
    private Account account = Account.getInstance();

    public TransactionTableModel() {
        populateTable();
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
    	if(columnIndex == 0) {
    		return data.get(rowIndex).getTransactionDate();
    	} else if (columnIndex == 1) {
            return data.get(rowIndex).getReceiptNumber();
        } else if (columnIndex == 2) {
            return data.get(rowIndex).isOnline();
        } else if (columnIndex == 3) {
            return data.get(rowIndex).getTaxTotal();
        } else if (columnIndex == 4) {
            return data.get(rowIndex).getTotal();
        } else if (columnIndex == 5) {
        	return data.get(rowIndex).getGrandTotal();
        } else if (columnIndex == 6) {
        	return data.get(rowIndex).getPaid();
        } else {
            return "Null";
        }
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Class getColumnClass(int column) {
        switch (column) {
        	case 0:
        		return Date.class;
            case 1:
                return String.class;
            case 2:
                return Boolean.class;
            case 3:
                return Double.class;
            case 4:
                return Double.class;
            case 5:
            	return Double.class;
            case 6:
            	return Double.class;
            default:
                return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    public void addRowFromTable(Transaction trans) {
        data.add(trans);
        account.addTransaction(trans);
        this.fireTableDataChanged();
    }

    public void deleteRow(int selectedRow) {
        data.remove(selectedRow);
        this.fireTableDataChanged();
    }

	@Override
	public void populateTable() {
		data = account.getTransactionList();
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		// Not implemented because cells can't be edited;
	}

	@Override
	public void saveData() {
		// TODO Auto-generated method stub
		
	}
}
