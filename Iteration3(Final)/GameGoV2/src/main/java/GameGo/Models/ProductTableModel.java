package GameGo.Models;

import GameGo.Objects.ESRBRating;
import GameGo.Objects.Game;
import GameGo.Objects.GameSystem;
import GameGo.Objects.Product;

import javax.swing.table.AbstractTableModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Logger;

public class ProductTableModel extends ConcreteTableModel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String[] columnNames = {"Name", "Price", "Quantity", "Rating"};
    private ArrayList<Product> data;
    final private Logger logger = Logger.getLogger(ProductTableModel.class.getName());
    public ProductTableModel() {
    	data = new ArrayList<Product>();
    	dataFile = new File("game_data.csv");
        populateTable();
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return data.get(rowIndex).getName();
        } else if (columnIndex == 1) {
            return data.get(rowIndex).getPrice();
        } else if (columnIndex == 2) {
            return data.get(rowIndex).getQuantity();
        } else if (columnIndex == 3) {
            return data.get(rowIndex).getConsumerRating();
        } else {
            return "Null";
        }
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Class getColumnClass(int column) {
        switch (column) {
            case 0:
                return String.class;
            case 1:
                return Double.class;
            case 2:
                return Integer.class;
            case 3:
                return Double.class;
            default:
                return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    public void addRowFromTable(Game game) {
        data.add(game);
        this.fireTableDataChanged();
    }

    public void deleteRow(int selectedRow) {
        data.remove(selectedRow);
        this.fireTableDataChanged();
    }

	@Override
	public void populateTable() {
		// TODO Auto-generated method stub
		Scanner reader = null;
		
		try {
			reader = new Scanner(dataFile);
			if(reader.hasNext()) {
				reader.nextLine();
			}
			
			while(reader.hasNext()) {
				String [] info = reader.nextLine().split(",");
				Product p = new Game();
				p.setName(info[0]);
				p.setPrice(Double.valueOf(Double.parseDouble(info[1])));
				p.setQuantity(Integer.parseInt(info[2]));
				((Game)p).setRating(ESRBRating.valueOf(info[3]));
				((Game)p).setConsoleType(info[4]);
				
				data.add(p);
			}
			
			dataFile = new File("console_data");
			
			reader = new Scanner(dataFile);
			if(reader.hasNext()) {
				reader.nextLine();
			}
			
			while(reader.hasNext()) {
				String [] info = reader.nextLine().split(",");
				Product p = new GameSystem();
				p.setName(info[0]);
				p.setPrice(Double.valueOf(Double.parseDouble(info[1])));
				p.setQuantity(Integer.parseInt(info[2]));
				
				data.add(p);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			String error = "";
			for(int i = 0; i < e.getStackTrace().length; i++) {
				error += e.getStackTrace()[i];
			}
			logger.warning(error);
		} finally {
			if(reader != null) {
				reader.close();
			}
		}
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveData() {
		// TODO Auto-generated method stub
		
	}
}
