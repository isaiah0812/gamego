package GameGo.Models;

import GameGo.Objects.Customer;
import GameGo.Objects.Employee;

import javax.swing.table.AbstractTableModel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class CustomerTableModel extends AbstractTableModel {
    private String[] columnNames = {"Name", "Email", "Phone Number", "Address", "Gender", "Card Number", "DOB"};
    private ArrayList<Customer> data;

    public CustomerTableModel(ArrayList<Customer> data) {
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return (data.get(rowIndex).getFirstName() + " " + data.get(rowIndex).getLastName());
        } else if (columnIndex == 1) {
            return data.get(rowIndex).getEmail();
        } else if (columnIndex == 2) {
            return data.get(rowIndex).getPhoneNumber();
        } else if (columnIndex == 3) {
            return data.get(rowIndex).getAddress();
        } else if (columnIndex == 4) {
            return data.get(rowIndex).getGender().toString();
        } else if (columnIndex == 5) {
            return data.get(rowIndex).getCard().getCardNumber();
        } else if (columnIndex == 6) {
            SimpleDateFormat fDate = new SimpleDateFormat("MM/dd/yy");
            return fDate.format(data.get(rowIndex).getDob());
        } else {
            return "NULL";
        }
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Class getColumnClass(int column) {
//        switch (column) {
//            case 0:
//                return String.class;
//            case 1:
//                return String.class;
//            case 2:
//                return String.class;
//            case 3:
//                return String.class;
//            case 4:
//                return String.class;
//            case 5:
//                return String.class;
//            default:
//                return String.class;
//        }
        return String.class;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    public void addRowFromTable(Customer person) {
        if(person != null) {
        	data.add(person);
        } else {
        	throw new NullPointerException();
        }
        this.fireTableDataChanged();
    }

    public void deleteRow(int selectedRow) {
        data.remove(selectedRow);
        this.fireTableDataChanged();
    }
}
