package GameGo.Objects;

public enum Gender {
	MALE("Male"),
	FEMALE("Female"),
	NOT_SPECIFIED("Not Specified");
	
	@SuppressWarnings("unused")
	private String name;
	
	Gender(String name) {
		this.name = name;
	}
}
