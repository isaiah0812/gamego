package GameGo.Objects;

import java.util.Date;
import java.util.logging.Logger;

import GameGo.Exceptions.*;

public class Customer extends Person {
	private Card card;
	private Logger logger = Logger.getLogger(Person.class.getName());
	
	public Customer(String e, String f, String l, String d, String p, 
					String aSN, String aC, String aS, String aZ, String g, 
					String cCN, String cC, String cDC, String cED, String cP, String cZC) 
													throws GenderException, StateException{

		if(this.verifyEmail(e)) {
			try {
				this.setEmail(e);
			} catch (EmailException e1) {
				// TODO Auto-generated catch block
				logger.warning(e1.getMessage());
			}
		} else {
			try {
				this.setEmail(null);
			} catch (EmailException e1) {
				// TODO Auto-generated catch block
				logger.warning(e1.getMessage());
			}
		}
		
		this.setFirstName(f);
		this.setLastName(l);
		@SuppressWarnings("deprecation")
		Date datey = new Date(Date.parse(d));
		this.setDob(datey);
		this.setPhoneNumber(p);
		Address addy = new Address();
		
		addy.setStreetName(aSN);
		addy.setCity(aC);
		addy.setState(null);
		for(int a = 0; a < State.values().length; a++) {
			if(aS.contains((State.values()[a].toString()))) {
				addy.setState(State.values()[a]);
				break;
			}
		}
		addy.setZip(aZ.substring(0, aZ.length()-1));
		switch(g.toLowerCase()) {
			case "male": this.setGender(Gender.MALE);
						 break;
			case "female": this.setGender(Gender.FEMALE);
						 break;
			case "not specified": this.setGender(Gender.NOT_SPECIFIED);
						 break;
			default: throw new GenderException();
		}
		
		if(addy.getState() == null) {
			throw new StateException();
		}
		
		this.setAddress(addy);
		
		Card cardy = new Card();
		cardy.setCardNumber(cCN.substring(1));
		cardy.setExpDate(cED);
		cardy.setCvcCode(cC);
		if(cDC.equals("yes")) {
			cardy.setDebitCard(true);
		} else {
			cardy.setDebitCard(false);
		}
		cardy.setPin(cP);
		cardy.setZipCode(cZC.substring(0, cZC.length()-1));
		this.setCard(cardy);
	}

	public Customer(){
		super();
		card = new Card();
	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}
	
	public void reportIssue() {
		
	}

	@Override
	public String toString() {
		return "Customer [getDob()=" + getDob() + ", getEmail()=" + getEmail() + ", getFirstName()=" + getFirstName()
				+ ", getLastName()=" + getLastName() + ", getPhoneNumber()=" + getPhoneNumber() + ", getAddress()="
				+ getAddress() + ", getGender()=" + getGender() + "]";
	}

	
	
	
}
