package GameGo.Objects;

public class Creator {
	private String name;
	private String phoneNumber;
	
	Creator(String Name, String PhoneNumber){
		setName(Name);
		setPhoneNumber(PhoneNumber);
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
