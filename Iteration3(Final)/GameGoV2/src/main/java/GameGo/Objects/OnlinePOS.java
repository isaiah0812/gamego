package GameGo.Objects;

import GameGo.Exceptions.POSException;

public class OnlinePOS extends POS {

	/**
	 * Class not implemented.
	 * "Online" Purchases and returns not available
	 */
	@Override
	public void startTransaction() throws POSException{
		
	}

	@Override
	public double finishTransaction(double pay) {
		return 0;
		
	}

}
