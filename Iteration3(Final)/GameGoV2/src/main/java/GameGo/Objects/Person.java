package GameGo.Objects;

import java.util.Date;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import GameGo.Exceptions.*;

public abstract class Person {
	private Date dob;
	private String email;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private Address address;
	private Gender gender;
	private static Pattern EMAIL_PATTERN = Pattern.compile(
			"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Za-z]{2,6}$", Pattern.CASE_INSENSITIVE);
	private static Pattern PHONE_NUMBER_PATTERN = Pattern.compile("^[0-9]{10}$");
	private Logger logger = Logger.getLogger(Person.class.getName());

	public Person() {
		this.firstName = "John";
		this.lastName = "Smith";
		this.dob = new Date();
		this.phoneNumber = "111111111111";
		this.address = new Address();
		this.gender = Gender.MALE;
	}

	public Person(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.dob = new Date();
		this.phoneNumber = "111111111111";
		this.address = new Address();
		this.gender = Gender.MALE;
	}

	final public Date getDob() {
		return dob;
	}
	final public void setDob(Date dob) {
		this.dob = dob;
	}
	final public String getEmail() {
		return email;
	}
	final public void setEmail(String email) throws EmailException {
		if(Person.verifyEmail(email)) {
			this.email = email;
		}
		else {
			logger.warning(email + " not valid");
			throw new EmailException();
		}

	}
	final public String getFirstName() {
		return firstName;
	}
	final public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	final public String getLastName() {
		return lastName;
	}
	final public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	final public String getPhoneNumber() {
		return phoneNumber;
	}
	final public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	final public Address getAddress() {
		return address;
	}
	final public void setAddress(Address address) {
		this.address = address;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	final public static boolean verifyEmail(String email) {
		Matcher m = EMAIL_PATTERN.matcher(email);
		if(m.matches()) {
			return true;
		}
		return false;
	}
	
	final public static boolean verifyPhoneNumber(String phoneNumber) {
		Matcher m = PHONE_NUMBER_PATTERN.matcher(phoneNumber);
		if(m.matches()) {
			return true;
		}
		return false;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((dob == null) ? 0 : dob.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (dob == null) {
			if (other.dob != null)
				return false;
		} else if (!dob.equals(other.dob))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (gender != other.gender)
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		return true;
	}
	
	public void clockIn() {}
	
	public void clockOut() {}
}
