package GameGo.Objects;

public class Game extends Product {
	ESRBRating rating;
	String consoleType;
	String serialNumber;

	public Game(){
		super();
	}

	public Game(String Name, Double Price, int Quantity, Double Rating, ESRBRating esrbRating, String imageFileName, String console){
		super(Name, Price, Quantity, Rating, imageFileName);
		rating = esrbRating;
		consoleType = console;
		serialNumber = generateSerialNumber();
	}

	// format - 000-000-XXX
	// meaning - Type-Quantity(Place in GameGo.Objects.Inventory Slots)-letters(first 3 letters of name)
	// Type {0 = PC, 1 = XBOX, 2 = PS4}
	private String generateSerialNumber() {
		String type = "000";
		switch(getConsoleType()) {
		case "XBOX":
			type = "001";
			break;
		case "PS4":
			type = "002";
			break;
		case "PC":
			type = "000";
			break;
			default:
				type = "000";
				break;
		}

		String quantity = String.valueOf(this.getQuantity());
		while(quantity.length() < 3) quantity = "0" + quantity;
		String letters = "";
		if(this.getName().length() > 2) {
			letters = this.getName().substring(0, 3).toUpperCase();
		} else {
			letters = this.getName();
			while(letters.length() < 3) letters = letters + "X";
		}

		return type + "-" +	quantity + "-" + letters;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public String getConsoleType() {
		return consoleType;
	}

	public void setConsoleType(String consoleType) {
		this.consoleType = consoleType;
		this.serialNumber = generateSerialNumber();
	}

	public ESRBRating getRating() {
		return rating;
	}

	public void setRating(ESRBRating rating) {
		this.rating = rating;
	}
}
