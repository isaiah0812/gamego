package GameGo.Objects;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.logging.Logger;

import GameGo.Exceptions.EmailException;

public class CustomerLine {
	final private Logger logger = Logger.getLogger(CustomerLine.class.getName());
    public Customer add(String line) throws ParseException {
        Customer tmp = new Customer();
        @SuppressWarnings("resource")
		Scanner lineScanner = new Scanner(line);
        lineScanner.useDelimiter(",");
        tmp.setFirstName(lineScanner.next());
        tmp.setLastName(lineScanner.next());
        String emailString = lineScanner.next();
        if (emailString.matches("^.+@\\w+.\\D{2,}$")) {
            try {
				tmp.setEmail(emailString);
			} catch (EmailException e) {
				// TODO Auto-generated catch block
				String error = "";
				for(int i = 0; i < e.getStackTrace().length; i++) {
					error += e.getStackTrace()[i];
				}
				logger.warning(error);
			}
        }
        tmp.setPhoneNumber(lineScanner.next());
        tmp.setAddress(new Address(lineScanner.next()));
        String genderString = lineScanner.next();
        if (genderString.toLowerCase().equals("male")) {
            tmp.setGender(Gender.MALE);
        } else if (genderString.toLowerCase().equals("female")) {
            tmp.setGender(Gender.FEMALE);
        } else {
            tmp.setGender(Gender.NOT_SPECIFIED);
        }
        tmp.setCard(new Card(lineScanner.next()));
        tmp.setDob(new SimpleDateFormat("MM/dd/yy").parse(lineScanner.next()));

        return tmp;
    }
}
