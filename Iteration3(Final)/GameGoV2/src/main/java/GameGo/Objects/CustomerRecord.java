//package GameGo.Objects;
//
//import GameGo.Models.ConcreteTableModel;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.Date;
//import java.util.Scanner;
//
//import GameGo.Exceptions.*;
//
//public class CustomerRecord extends ConcreteTableModel {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//
//	public CustomerRecord(File dataFile) {
//		super(dataFile);
//		// GameGo.Objects.Address, City, GameGo.Objects.State, and Zip are combined in file as just GameGo.Objects.Address
//		// GameGo.Objects.Card Number, Expiration Date, CVC, Debit Card?, Pin Number, and Card Zip Code are combined in file as just Card
//		String [] cn = {"Email", "First Name", "Last Name", "DOB", "Phone Number", "Address", "City", "Texas", "Zip", "Gender", "Card Number", "Expiration Date", "CVC", "Debit Card?", "Pin Number", "Card Zip Code"};
//		this.columnNames = cn;
//	}
//
//	@Override
//	public boolean isCellEditable(int row, int col) {
//		// TODO Auto-generated method stub
//		return true;
//	}
//
//	@Override
//	public void populateTable() {
//		// TODO Auto-generated method stub
//		Scanner in = null;
//		try {
//			in = new Scanner(dataFile);
//			if(in.hasNext()) {
//				in.nextLine();
//			}
//			
//			while(in.hasNext()) {
//				String [] info = in.nextLine().split(",");
//				
//				if(info.length == 16) {
//					this.data.add(info);
//				}
//				else {
//					System.err.println("Data missing");
//				}
//			}
//		} catch(IOException e) {
//			System.err.println(e);
//		} finally {
//			if(in != null) {
//				in.close();
//			}
//		}
//	}
//
//	@Override
//	public void setValueAt(Object value, int row, int col) {
//		data.get(row)[col] = value.toString();
//	}
//
//	@Override
//	public void saveData() {
//		// TODO Auto-generated method stub
//		
//	}
//	
//	@SuppressWarnings("deprecation")
//	public Customer objectVersion(String email) throws PositionException, GenderException, EmailException, StateException {
//		Customer cus = null;
//		for(int i = 0; i < data.size(); i++) {
//			if(data.get(i)[0].equals(email)) {
//				cus = new Customer();
//				cus.setEmail(email);
//				cus.setFirstName(data.get(i)[1]);
//				cus.setLastName(data.get(i)[2]);		
//				cus.setDob(new Date(Date.parse(data.get(i)[3])));
//				cus.setPhoneNumber(data.get(i)[4]);
//				
//				Address addy = new Address();
//				addy.setStreetName(data.get(i)[5]);
//				addy.setCity(data.get(i)[6]);
//				addy.setState(null);
//				for(int a = 0; a < State.values().length; a++) {
//					if(data.get(i)[7].contains((State.values()[a].toString()))) {
//						addy.setState(State.values()[a]);
//						break;
//					}
//					
//				}
//				addy.setZip(data.get(i)[8].substring(0, data.get(i)[8].length()-1));
//				switch(data.get(i)[9].toLowerCase()) {
//					case "male": cus.setGender(Gender.MALE);
//								 break;
//					case "female": cus.setGender(Gender.FEMALE);
//								 break;
//					case "not specified": cus.setGender(Gender.NOT_SPECIFIED);
//								 break;
//					default: throw new GenderException();
//				}
//				
//				if(addy.getState() == null) {
//					throw new StateException();
//				}
//				cus.setAddress(addy);
//				
//				System.out.println(cus.getFirstName() + "\'s address info: " + addy.toString());
//				
//				Card cardy = new Card();
//				cardy.setCardNumber(data.get(i)[10].substring(1));
//				cardy.setExpDate(data.get(i)[11]);
//				cardy.setCvcCode(data.get(i)[12]);
//				if(data.get(i)[13].equals("yes")) {
//					cardy.setDebitCard(true);
//				} else {
//					cardy.setDebitCard(false);
//				}
//				cardy.setPin(data.get(i)[14]);
//				cardy.setZipCode(data.get(i)[15].substring(0, data.get(i)[15].length()-1));
//				cus.setCard(cardy);
//				
//				System.out.println(cus.getFirstName() + "\'s card info: " + cardy.toString());
//				
//				break;
//			}
//		}
//		
//		return cus;
//	}
//
//}
