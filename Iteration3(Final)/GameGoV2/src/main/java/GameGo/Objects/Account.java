package GameGo.Objects;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.sun.istack.logging.Logger;

public class Account {
	private Double equity;
	private Double onlineSales;
	private Double taxSales;
	private Double totalSales;
	private List<Transaction> transactionList;
	private static Account instance = null;
	final private static File mainFrame = new File(System.getProperty("user.dir") + "/src/main/resources/account.csv");
	@SuppressWarnings("unused")
	private Logger logger = Logger.getLogger(Account.class);
	@SuppressWarnings("deprecation")
	private Account() {
		taxSales = Double.valueOf(0);
		totalSales = Double.valueOf(0);
		equity = Double.valueOf(500000);
		transactionList = new ArrayList<Transaction>();
		Scanner reader = null;
		try {
			reader = new Scanner(mainFrame);
			if(reader.hasNext()) {
				reader.nextLine();
			}

			while(reader.hasNext()) {
				String [] info = reader.nextLine().split(",");
				Transaction t = new Transaction();
				t.setTransactionDate(new Date(Date.parse(info[0])));
				t.setReceiptNumber(info[1]);
				t.setOnline(Boolean.valueOf(info[2]).booleanValue());
				t.setTaxTotal(Double.parseDouble(info[3]));
				t.setTotal(Double.parseDouble(info[4]));
				t.setPaid(Double.parseDouble(info[6]));
				if(Transaction.validateReceiptNumber(t.getReceiptNumber())
						&& !findTransaction(t.getReceiptNumber())) {
					transactionList.add(t);
					equity += t.getTotal();
					totalSales += t.getGrandTotal();
					taxSales += t.getTaxTotal();
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			String error = "";
			for(int i = 0; i < e.getStackTrace().length; i++) {
				error += e.getStackTrace()[i];
			}
			logger.warning(error);
		} finally {
			if(reader != null) {
				reader.close();
			}
		}
	}
	
	public static Account getInstance() {
		if(instance == null) {
			instance = new Account();
		}
		
		return instance;
	}

	public void addTransaction(Transaction t) {
		transactionList.add(t);
		
		equity += t.getTotal();
		totalSales += t.getGrandTotal();
		taxSales += t.getTaxTotal();
		PrintWriter writer = null;
		// Saving transaction to file
		try {
			writer = new PrintWriter(mainFrame);
			
//			String add = String.join(",", t.getReceiptNumber(), 
//										  String.valueOf(t.isOnline()), 
//										  String.valueOf(t.getTaxTotal()), 
//										  String.valueOf(t.getTotal()), 
//										  String.valueOf(t.getGrandTotal()), 
//										  String.valueOf(t.getPaid()));
//			writer.append(add);
			
			writer.println("Transaction Date,Receipt #,Online?,Tax Total,Subtotal,Grand Total,Customer Payment");
			
			for(Transaction t1: transactionList) {
				String add = String.join(",", new SimpleDateFormat("mm/dd/yyyy").format(t1.getTransactionDate()),
											  t1.getReceiptNumber(), 
											  String.valueOf(t1.isOnline()), 
											  String.valueOf(t1.getTaxTotal()), 
											  String.valueOf(t1.getTotal()), 
											  String.valueOf(t1.getGrandTotal()), 
											  String.valueOf(t1.getPaid()));
				writer.println(add);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			logger.info(e.getStackTrace().toString());
		} finally {
			if(writer != null) {
				writer.close();
			}
		}
	}

	public double voidTransaction(String receipt){
		Transaction t = null;
		double returnVal = -1;
		
		for(Transaction t1: transactionList) {
			if(t1.getReceiptNumber().equals(receipt)) {
				t = t1;
				break;
			}
		}
		if(t != null) {
			returnVal = 0;
			taxSales -= t.getTaxTotal();
			returnVal += t.getTaxTotal();
			if(t.isOnline()) {
				onlineSales -= t.getGrandTotal();
			} else {
				totalSales -= t.getGrandTotal();
			}
			
			returnVal += t.getTaxTotal();
			transactionList.remove(t);
		}
		
		return returnVal;
	}
	
	public boolean findTransaction(String receipt) {
		boolean found = false;
		
		for(Transaction t: transactionList) {
			if(t.getReceiptNumber().equals(receipt)) {
				found = true;
				break;
			}
		}
		
		return found;
	}

	public Double getEquity() {
		return equity;
	}
	public void setEquity(Double equity) {
		this.equity = equity;
	}
	public Double getOnlineSales() {
		return onlineSales;
	}
	public void setOnlineSales(Double onlineSales) {
		this.onlineSales = onlineSales;
	}
	public Double getTaxSales() {
		return taxSales;
	}
	public void setTaxSales(Double taxSales) {
		this.taxSales = taxSales;
	}
	public Double getTotalSales() {
		return totalSales;
	}
	public void setTotalSales(Double totalSales) {
		this.totalSales = totalSales;
	}
	public List<Transaction> getTransactionList() {
		return transactionList;
	}
	public void setTransactionList(List<Transaction> transactionList) {
		this.transactionList = transactionList;
	}
	
	public void payEmployee(Employee e) {
		if(e.getPosition() == Position.MANAGER) {
			equity -= (e.getPayRate() * 1000)/52;
		}
		else {
			equity -= e.getPayRate() * e.getTotalHours();
		}
		logger.info(e.getEmployeeID() + "(" + e.getLastName() + ", " + e.getFirstName() + ") was paid");
	}
}
