package GameGo.Objects;

import GameGo.Exceptions.POSException;

public abstract class POS {
	private int id;
	protected Transaction currentTrans;
	final protected Account account = Account.getInstance();

	final public int getId() {
		return id;
	}

	final public void setId(int id) {
		this.id = id;
	}
	
	final public Transaction getCurrentTrans() {
		return currentTrans;
	}

	final public void setCurrentTrans(Transaction currentTrans) {
		this.currentTrans = currentTrans;
	}

	public abstract void startTransaction() throws POSException;
	
	public abstract double finishTransaction(double pay);
}