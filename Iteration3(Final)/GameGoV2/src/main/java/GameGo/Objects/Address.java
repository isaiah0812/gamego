package GameGo.Objects;

public class Address {
	private String city;
	private State state;
	private String streetName;
	private String zip;
	
	public Address() {
		
	}
	
	public Address(String str, String c, String sta, String z) {
		this.city = c;
		this.state = State.valueOf(sta);
		this.streetName = str;
		this.zip = z;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public boolean equals(Address that) {
		boolean ret = true;
		
		if(!this.streetName.equals(that.streetName)) {
			ret = false;
		}
		
		if(!this.city.equals(that.city)) {
			ret = false;
		}
		
		if(this.state != that.state) {
			ret = false;
		}
		
		if(!this.zip.equals(that.zip)) {
			ret = false;
		}
		
		return ret;
	}	


	public Address(String streetName) {
		this.streetName = streetName;
		this.city = "Waco";
		this.state = State.TX;
		this.zip = "76706";
	}

	@Override
	public String toString() {
		return (streetName + ", " + city + ", " + state + ", " + zip);
	}
}
